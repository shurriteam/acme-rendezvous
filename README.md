# ACME-Rendezvous #

Acme, Inc. is a holding that encompasses many companies worldwide. One of them is Acme Rendezvous, Inc., which specialises in helping people organise rendezvouses.

The goal of this project is to develop a web information system to help Acme Rendezvous, Inc. manage their business. This document provides an informal description of their requirements; ask your lecturers for clarifications and details.

### Requisitos de información ###

The system must support two kinds of actors, namely: administrators and users. It must store the following information regarding them: their names, surnames, postal addresses (optional), phone numbers (optional), and email addresses.

Users can create rendezvouses. For each rendezvous, the system must store its name, its description, the moment when it’s going to be organised, an optional picture, optional GPS co-ordinates, and the creator and the list of attendants.

The system must handle comments about the rendezvouses. For every comment the system must store the user who wrote it, the moment when it was written, the corresponding text, and an optional picture that is referenced by means of a URL.

Rendezvouses may have announcements. The system must record the moment when an announcement is made, plus a title and a description.

A rendezvous may be explicitly linked to similar ones by its creator. Note that such links may be added or removed even if the rendezvous is saved in final mode. They must be listed whenever a rendezvous is shown.

Some rendezvouses may be flagged as “adult only”, in which case every attempt to RSVP them by users who are under 18 must be prohibited. Such rendezvouses must not be displayed unless the user who is browsing them is at least 18 year old. Obviously, they must not be shown to unauthenticated users.

The creator of a rendezvous may associate a number of questions with it, each of which must be answered when a user RSVPs that rendezvous.

In addition to writing a comment from scratch, a user may reply to a comment.









