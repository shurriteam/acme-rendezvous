package services;


import domain.Announcement;
import domain.RequestForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.RequestFormRepository;

import java.util.Collection;

@Service
@Transactional
public class RequestFormService  {

    // Constructors--------------------------------------------------------------------------------------

   @Autowired
    private RequestFormRepository requestFormRepository;

   public RequestFormService () {super();}



    // Simple CRUD method --------------------------------------------------------------------------------

    public RequestForm create() {

        RequestForm res = new RequestForm();
        return res;
    }
    public Collection<RequestForm> findAll() {
        Collection<RequestForm> res = requestFormRepository.findAll();
        Assert.notNull(res);
        return res;
    }


    public RequestForm findOne(int Comment) {
        RequestForm res = requestFormRepository.findOne(Comment);
        Assert.notNull(res);
        return res;
    }

    public RequestForm save(RequestForm r) {
        Assert.notNull(r);

        RequestForm res = requestFormRepository.save(r);
        return res;
    }

    public void delete(RequestForm r) {
        Assert.notNull(r);
        Assert.isTrue(r.getId() != 0);
        requestFormRepository.delete(r);
    }

}
