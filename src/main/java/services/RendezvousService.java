/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.RendezvousRepository;

import java.util.Collection;

@Service
@Transactional
public class RendezvousService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private RendezvousRepository rendezvousRepository;

   // Managed repository--------------------------------------------------------------------------------

   public RendezvousService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Rendezvous create() {

      Rendezvous res = new Rendezvous();
      return res;
   }

   public Collection<Rendezvous> findAll() {
      Collection<Rendezvous> res = rendezvousRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Rendezvous findOne(int Comment) {
      Rendezvous res = rendezvousRepository.findOne(Comment);
      Assert.notNull(res);
      return res;
   }

   public Rendezvous save(Rendezvous a) {
      Assert.notNull(a);
      Rendezvous res = rendezvousRepository.save(a);
      return res;
   }

   public void delete(Rendezvous a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      rendezvousRepository.delete(a);
   }

   public void flush() {
      rendezvousRepository.flush();
   }

   // Other business methods -------------------------------------------------------------------------------


   public Collection<Announcement> announcementsGivenARendez(int rendezId) {
      return rendezvousRepository.announcementsGivenARendez(rendezId);
   }

   public Collection<Rendezvous> relatedRendezsGivenARendez(int rendezId) {
      return rendezvousRepository.relatedRendezsGivenARendez(rendezId);
   }

   public Collection<Servicee> relatedRendezsGivenAServicee(int rendezId) {
      return rendezvousRepository.relatedRendezsGivenAServicee(rendezId);
   }

   public Collection<Question> relatedQuestionsGivenARendez(int rendezId) {
      return rendezvousRepository.relatedQuestionsGivenARendez(rendezId);
   }

   public Form formWithGivenUer(int userId, int rendezvousId) {
	   Assert.notNull(userId);
	   Assert.notNull(rendezvousId);
	   return rendezvousRepository.formwithGivenUser(userId, rendezvousId);
   }

   public Collection<Form> relatedFormsGivenARendez(int rendezId) {
      return rendezvousRepository.relatedFormsGivenARendez(rendezId);
   }

   public Collection<Rendezvous> rendezvousesForAdults() {
      return rendezvousRepository.rendezForAdults();
   }

   public Collection<Rendezvous> rendezvousesForAllPublics() {
      return rendezvousRepository.rendezForAllAges();
   }
}



