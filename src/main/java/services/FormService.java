/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Form;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.FormRepository;

import java.util.Collection;

@Service
@Transactional
public class FormService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private FormRepository formRepository;

   // Managed repository--------------------------------------------------------------------------------

   public FormService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Form create() {

      Form res = new Form();
      return res;
   }

   public Collection<Form> findAll() {
      Collection<Form> res = formRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Form findOne(int Comment) {
      Form res = formRepository.findOne(Comment);
      Assert.notNull(res);
      return res;
   }

   public Form save(Form a) {
      Assert.notNull(a);
      Form res = formRepository.save(a);
      return res;
   }

   public void delete(Form a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      formRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------

   public void flush() {
      formRepository.flush();
   }

}



