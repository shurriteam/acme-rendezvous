/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Actor;
import domain.Announcement;
import domain.Rendezvous;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.UserRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.Collection;

@Service
@Transactional
public class UserService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private UserRepository userRepository;
   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private AdministratorService administratorService;
@Autowired
private  CreditCardService creditCardService;


   // Managed repository--------------------------------------------------------------------------------


   // Suporting services --------------------------------------------------------------------------------


   public UserService() {
      super();
   }


   // Simple CRUD method --------------------------------------------------------------------------------

   public User create() {
      User res = new User();
      return res;
   }

   public Collection<User> findAll() {
      Collection<User> res = userRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public User findOne(int User) {
      domain.User res = userRepository.findOne(User);
      Assert.notNull(res);
      return res;
   }

   public User save(User a) {
      Assert.notNull(a);
      User res = userRepository.save(a);
      return res;
   }

   public void delete(User a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      userRepository.delete(a);

   }

   public void flush() {
      userRepository.flush();
   }

   // Other business methods -------------------------------------------------------------------------------


   public User findByPrincipal() {
      User result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   private User findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);
      User result;
      result = userRepository.findByUserAccountId(userAccount.getId());
      return result;
   }

   public Actor registerAsUser(User u) {
      User resu = null;
      try {
         Assert.notNull(u);
         Authority autoh = new Authority();
         autoh.setAuthority("USER");
         UserAccount res = new UserAccount();
         res.addAuthority(autoh);
         res.setUsername(u.getUserAccount().getUsername());
         Md5PasswordEncoder encoder;
         encoder = new Md5PasswordEncoder();
         String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
         res.setPassword(hash);
         UserAccount userAccount = userAccountService.save(res);
         u.setUserAccount(userAccount);

         userRepository.save(u);
         Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      } catch (Exception e) {
         e.printStackTrace();
      }

      return resu;
   }

   public Collection<Rendezvous> createdRenezByUser(int userId) {
      Assert.notNull(userId);
      return userRepository.createdRenezByUser(userId);

   }

   public Collection<Announcement> createdAnnouncementsByUser(int userId) {
      Assert.notNull(userId);
      return userRepository.createdAnnouncementsByUser(userId);
   }
   
   public Collection<Announcement> annoucmentsInbox(int userId){
	   Assert.notNull(userId);
	   return userRepository.allMyAnnouncement(userId);
   }

}




