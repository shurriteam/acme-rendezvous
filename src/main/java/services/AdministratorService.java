/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import javax.naming.OperationNotSupportedException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import static org.hibernate.jpa.internal.QueryImpl.LOG;

/**
 * Created by daviddelatorre on 28/3/17.
 */
@Service
@Transactional
public class AdministratorService {

   Logger log = Logger.getLogger("com.nullpoint.acme");
   // Managed Repository ------------------------
   @Autowired
   public AdministratorRepository administratorRepository;
   @Autowired
   public CommentService commentService;
   @Autowired
   public RendezvousService rendezvousService;
   @Autowired
   public AnnouncementService announcementService;

   // Supporting services -----------------------

   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private ActorService actorService;

   public AdministratorService() {
      super();
   }

   // Simple CRUD methods -----------------------
   public Administrator create() {
      Administrator res;
      res = new Administrator();
      return res;
   }

   public Administrator findOne(int actorId) {
      Administrator result;

      result = administratorRepository.findOne(actorId);

      return result;
   }

   public Collection<Administrator> findAll() {
      Collection<Administrator> result;

      result = administratorRepository.findAll();

      return result;
   }

   public Administrator save(Administrator actor) {
      Assert.notNull(actor);
      return administratorRepository.save(actor);
   }

   public void delete(Administrator actor) {
      Assert.notNull(actor);
      Assert.isTrue(administratorRepository.exists(actor.getId()));
      administratorRepository.delete(actor);
   }


   // Other business methods -----------------------


   public Administrator findByPrincipal() {
      Administrator result;
      UserAccount userAccount;

      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);

      return result;
   }

   private Administrator findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Administrator result;

      result = administratorRepository.findByUserAccountId(userAccount.getId());

      return result;
   }

   public void removeComment(Comment comment){

      try {
         Assert.notNull(comment);
         if (commentService.findAll().contains(comment)){
            log.info("Deleting comment");
            commentService.delete(comment);
         }else {

            throw new UnsupportedOperationException("The comment is already deleted");
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }


   public void removeRendezvouz(Rendezvous rendezvous){
      try {
         Assert.notNull(rendezvous);
         if (rendezvousService.findAll().contains(rendezvous)){
            log.info("Deleting rendezvouz");
            rendezvousService.delete(rendezvous);
         }else {
            throw new UnsupportedOperationException("The rendezvouz is already deleted");
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   public void removeAnnoucement(Announcement announcement){
      try {
         Assert.notNull(announcement);
         if (announcementService.findAll().contains(announcement)){
            log.info("Deleting announcement");
            announcementService.delete(announcement);
         }else {
            throw new UnsupportedOperationException("The announcement is already deleted");
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }


   public void flush() {
      administratorRepository.flush();
   }



   //DASHBOARD

      public Double averageOfRendezvousCreatedPerUser(){
         return  administratorRepository.averageOfRendezvousCreatedPerUser();
      }

      public Double standarDesviationOfRendezvousCreatedPerUser(){
         return  administratorRepository.standarDesviationOfRendezvousCreatedPerUser();
      }

      public Integer numberOfUsersWhoHasCreatedRendezvous(){
         return administratorRepository.numberOfUsersWhoHasCreatedRendezvous().size();
      }

      public Integer numberOfUsersWhoNeverCreatesRendezvous(){
         return administratorRepository.numberOfUsersWhoNeverCreatesRendezvous().size();
      }

      public Double averageNumberOfUsersPerRendezvous(){
         return administratorRepository.averageNumberOfUsersPerRendezvous();
      }

      public Double standarDesviationOfUserPerRendezvous(){
         return  administratorRepository.standarDesviationOfUserPerRendezvous();
      }

      public Double averageOfRendezvousRSVPPerUser(){
         return administratorRepository.averageOfRendezvousRSVPPerUser();
      }

      public Double standarDesviationOfRendesvousRSVPerUser(){
         return administratorRepository.standarDesviationOfRendesvousRSVPerUser();
      }

      public List<Rendezvous> topTenRendezvousWithMoreRSVP(){
         List<Rendezvous> rendezvous = new ArrayList<>(administratorRepository.topTenRendezvousWithMoreRSVP());
         if(rendezvous.size()>=10){
            List<Rendezvous> res = rendezvous.subList(0,10);
            return res;
         }else {
            return rendezvous;

         }



      }

      public Double averageOfAnnoucmentPerRendezvous(){
         return administratorRepository.averageOfAnnoucmentPerRendezvous();
      }

      public Double standarDesviationOfAnnoucementPerRendezvous(){
         return administratorRepository.standarDesviationOfAnnoucementPerRendezvous();
      }

      public Collection<Rendezvous> rendezvousWithMoreThan75PerCentOfAnnoucmenetThatTheAverage(){
         return administratorRepository.rendezvousWithMoreThan75PerCentOfAnnoucmenetThatTheAverage();
      }

      public Double averageNumberOfLinkedInRendezvous(){
         return administratorRepository.averageNumberOfLinkedInRendezvous();
      }

      public Collection<Rendezvous> rendezvousWithMoreLinkThanTheAveragePlusTen(){
         return administratorRepository.rendezvousWithMoreLinkThanTheAveragePlusTen();
      }

      public Double averageNumberOfQuestionsPerRendezvous(){
         return administratorRepository.averageNumberOfQuestionsPerRendezvous();
      }

      public Double standarDesviationOfQuestionsPerRendezvous(){
         return administratorRepository.standarDesviationOfQuestionsPerRendezvous();
      }

      public Double averageNumberOfAnswerPerRendezvous(){
         return administratorRepository.averageNumberOfAnswerPerRendezvous();
      }

      public Double standarDesviationOfAnswerPerRendezvous(){
         return administratorRepository.standarDesviationOfAnswerPerRendezvous();
      }

      public Double averageNumberOfRepliesForComment(){
         return administratorRepository.averageNumberOfRepliesForComment();
      }

      public Double standarDesviationOfRepliesForComments(){
         return administratorRepository.standarDesviationOfRepliesForComments();

      }
      public Servicee bestSellingservice(){

         List<Servicee> servicees = new ArrayList<>(administratorRepository.bestSellingservice());
         Servicee res = servicees.get(0);

         return res;
      }


      public Collection<Manager> ManagerWithMoreServicesThanAverage(){
         return administratorRepository.ManagerWithMoreServicesThanAverage();
      }


   public Collection<Manager> managerWithMoreServicesCancelled() {
         return administratorRepository.managerWithMoreservicesCancelled();
   }


   public Collection<Double> avgMinMaxDevServicesPerRendezvous() {
         return administratorRepository.avgMinMaxDevServicesPerRendezvous();
   }

   public Double averageNumberOfServicePerCategory(){
      return administratorRepository.averageNumberOfServicePerCategory();

   }

   public Collection<Servicee> topSellingServices(){
      return administratorRepository.topSellingServices();
   }
}
