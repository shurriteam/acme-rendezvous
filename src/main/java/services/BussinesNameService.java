package services;

import domain.BussinesName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.BussinesNameRepository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 12/3/17.
 */
@Service
@Transactional
public class BussinesNameService {

    // Constructors--------------------------------------------------------------------------------------

    public BussinesNameService() {
        super();
    }

    // Managed repository--------------------------------------------------------------------------------

    @Autowired
    private BussinesNameRepository bussinesNameRepository;


    // Suporting services --------------------------------------------------------------------------------

    // Simple CRUD method --------------------------------------------------------------------------------

    public BussinesName create() {
        BussinesName res;
        res = new BussinesName();
        return res;
    }

    public Collection<BussinesName> findAll() {
        Collection<BussinesName> res;
        res = bussinesNameRepository.findAll();
        Assert.notNull(res);
        return res;
    }

    public BussinesName findOne(int BussinesName) {
        domain.BussinesName res;
        res = bussinesNameRepository.findOne(BussinesName);
        Assert.notNull(res);
        return res;
    }

    public BussinesName save(BussinesName a) {
        Assert.notNull(a);
        BussinesName res;
        res = bussinesNameRepository.save(a);
        return res;
    }

    public void delete(BussinesName a) {
        Assert.notNull(a);
        Assert.isTrue(a.getId() != 0);
        bussinesNameRepository.delete(a);
    }

    // Other business methods -------------------------------------------------------------------------------
    public void flush(){
        bussinesNameRepository.flush();
    }
}
