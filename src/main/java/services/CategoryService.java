/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Category;

import domain.Servicee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.CategoryRepository;

import java.util.Collection;

@Service
@Transactional
public class CategoryService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private CategoryRepository categoryRepository;
   @Autowired
   private ActorService actorService;

   // Managed repository--------------------------------------------------------------------------------

   public CategoryService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Category create() {

	   Category res = new Category();
      return res;
   }

   public Collection<Category> findAll() {
      Collection<Category> res = categoryRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Category findOne(int Comment) {
      Assert.notNull(Comment);

      Category res = categoryRepository.findOne(Comment);
      return res;
   }

   public Category save(Category a) {
      Assert.notNull(a);
      Assert.isTrue( actorService.checkRole("ADMINISTRATOR"));
      Category res = categoryRepository.save(a);
      return res;
   }

   public void delete(Category a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      categoryRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------

   public void flush() {
      categoryRepository.flush();
   }

   public Category getCategoryUncategorize(){
      return categoryRepository.getUncategortidezCategory();
   }

   public void uncategorizedServices(Category category) {

      try {
         Collection<Servicee> services = category.getAssociatedServices();
         Category category1 = this.getCategoryUncategorize();
         for (Servicee servicee: services){
            servicee.setCategorie(category1);
         }
      } catch (Exception e) {
         e.printStackTrace();
      }

   }
}



