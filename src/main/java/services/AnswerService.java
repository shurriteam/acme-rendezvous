/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Announcement;
import domain.Answer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AnnouncementRepository;
import repositories.AnswerRepository;

import java.util.Collection;

@Service
@Transactional
public class AnswerService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private AnswerRepository answerRepository;

   // Managed repository--------------------------------------------------------------------------------

   public AnswerService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Answer create() {

	   Answer res = new Answer();
      return res;
   }

   public Collection<Answer> findAll() {
      Collection<Answer> res = answerRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Answer findOne(int Comment) {
      domain.Answer res = answerRepository.findOne(Comment);
      Assert.notNull(res);
      return res;
   }

   public Answer save(Answer a) {
      Assert.notNull(a);
      Answer res = answerRepository.save(a);
      return res;
   }

   public void delete(Answer a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      answerRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------

   public void flush() {
	      answerRepository.flush();
	   }
   
}



