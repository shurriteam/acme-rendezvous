/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import repositories.ActorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class ActorService {

   // Managed Repository ------------------------
   @Autowired
   private ActorRepository actorRepository;
   @Autowired
   private UserAccountService userAccountService;


   // Supporting services -----------------------

   // Constructor -------------------------------
   public ActorService() {
      super();
   }

   // Simple CRUD methods -----------------------

   public Actor findOne(int actorId) {
      Actor result;

      result = actorRepository.findOne(actorId);

      return result;
   }

   public Collection<Actor> findAll() {
      Collection<Actor> result;

      result = actorRepository.findAll();

      return result;
   }

   public Actor save(Actor actor) {
      Assert.notNull(actor);
      return actorRepository.save(actor);
   }

   public void delete(Actor actor) {
      Assert.notNull(actor);
      Assert.isTrue(actorRepository.exists(actor.getId()));
      actorRepository.delete(actor);
   }

   // Other business methods -----------------------


   public Actor findByPrincipal() {
      Actor result;
      UserAccount userAccount;

      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByUserAccount(userAccount);
      Assert.notNull(result);

      return result;
   }

   private Actor findByUserAccount(UserAccount userAccount) {
      Assert.notNull(userAccount);

      Actor result;

      result = actorRepository.findByUserAccountId(userAccount.getId());

      return result;
   }

   public void flush() {
      actorRepository.flush();
   }

   //Manage Folder

   boolean findByPrincipalHasRol(String rol) {
      boolean res = false;
      Assert.notNull(rol, "rol is null");

      Actor a = this.findByPrincipal();
      Assert.notNull(a, "actor is null");
      for (Authority at : a.getUserAccount().getAuthorities()) {
         if (at.getAuthority().equals(rol)) {
            res = true;
            break;
         }
      }
      return res;
   }


   public Actor actorByEmail(String email) {
      Actor res = actorRepository.findUserByEmail(email);
      Assert.notNull(res);
      return res;
   }
   public boolean isAdministrator() {
      boolean result;

      result = this.checkRole(Authority.ADMINISTRATOR);

      return result;
   }
   public boolean checkRole(final String role) {
      boolean result;
      Collection<Authority> authorities;

      result = false;
      authorities = LoginService.getPrincipal().getAuthorities();
      for (final Authority a : authorities)
         result = result || a.getAuthority().equals(role);

      return result;
   }
}