/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Actor;
import domain.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ManagerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;

import java.util.Collection;

@Service
@Transactional
public class ManagerService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private ManagerRepository managerRepository;
   @Autowired
   private UserAccountService userAccountService;

   // Managed repository--------------------------------------------------------------------------------

   public ManagerService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Manager create() {

	   Manager res = new Manager();

      return res;
   }

   public Collection<Manager> findAll() {
      Collection<Manager> res = managerRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Manager findOne(int Comment) {
      Manager res = managerRepository.findOne(Comment);
      Assert.notNull(res);
      return res;
   }

   public Manager save(Manager a) {
      Assert.notNull(a);
      Manager res = managerRepository.save(a);
      return res;
   }

   public void delete(Manager a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      managerRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------


   public void flush() {
      managerRepository.flush();
   }


   public Manager findByPrincipal() {
      Manager result;
      UserAccount userAccount;
      userAccount = LoginService.getPrincipal();
      Assert.notNull(userAccount);
      result = findByManagerAccount(userAccount);
      Assert.notNull(result);
      return result;
   }

   private Manager findByManagerAccount(UserAccount managerAccount) {
      Assert.notNull(managerAccount);
      Manager result;
      result = managerRepository.findByUserAccountId(managerAccount.getId());
      return result;
   }

   public Actor registerAsManager(Manager u) {
      Manager resu = null;
      try {
         Assert.notNull(u);
         Authority autoh = new Authority();
         autoh.setAuthority("MANAGER");
         UserAccount res = new UserAccount();
         res.addAuthority(autoh);
         res.setUsername(u.getUserAccount().getUsername());
         Md5PasswordEncoder encoder;
         encoder = new Md5PasswordEncoder();
         String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
         res.setPassword(hash);
         UserAccount managerAccount = userAccountService.save(res);
         u.setUserAccount(managerAccount);
         managerRepository.save(u);
         Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
      } catch (Exception e) {
         e.printStackTrace();
      }

      return resu;
   }



}



