package services;

import domain.WelcomePhrase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.WelcomePhraseRepository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 12/3/17.
 */
@Service
@Transactional
public class WelcomePhraseService {

    // Constructors--------------------------------------------------------------------------------------

    public WelcomePhraseService() {
        super();
    }

    // Managed repository--------------------------------------------------------------------------------

    @Autowired
    private WelcomePhraseRepository welcomePhraseRepository;


    // Suporting services --------------------------------------------------------------------------------

    // Simple CRUD method --------------------------------------------------------------------------------

    public WelcomePhrase create() {
        WelcomePhrase res;
        res = new WelcomePhrase();
        return res;
    }

    public Collection<WelcomePhrase> findAll() {
        Collection<WelcomePhrase> res;
        res = welcomePhraseRepository.findAll();
        Assert.notNull(res);
        return res;
    }

    public WelcomePhrase findOne(int WelcomePhrase) {
        domain.WelcomePhrase res;
        res = welcomePhraseRepository.findOne(WelcomePhrase);
        Assert.notNull(res);
        return res;
    }

    public WelcomePhrase save(WelcomePhrase a) {
        Assert.notNull(a);
        WelcomePhrase res;
        res = welcomePhraseRepository.save(a);
        return res;
    }

    public void delete(WelcomePhrase a) {
        Assert.notNull(a);
        Assert.isTrue(a.getId() != 0);
        welcomePhraseRepository.delete(a);
    }

    // Other business methods -------------------------------------------------------------------------------
    public void flush(){
        welcomePhraseRepository.flush();
    }
}
