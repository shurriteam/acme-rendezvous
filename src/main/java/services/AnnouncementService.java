/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Announcement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AnnouncementRepository;

import java.util.Collection;

@Service
@Transactional
public class AnnouncementService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private AnnouncementRepository announcementRepository;

   // Managed repository--------------------------------------------------------------------------------

   public AnnouncementService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Announcement create() {

      Announcement res = new Announcement();
      return res;
   }

   public Collection<Announcement> findAll() {
      Collection<Announcement> res = announcementRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Announcement findOne(int Comment) {
      domain.Announcement res = announcementRepository.findOne(Comment);
      Assert.notNull(res);
      return res;
   }

   public Announcement save(Announcement a) {
      Assert.notNull(a);
      Announcement res = announcementRepository.save(a);
      return res;
   }

   public void delete(Announcement a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      announcementRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------

}



