/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Announcement;
import domain.Question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AnnouncementRepository;
import repositories.QuestionRepository;

import java.util.Collection;

@Service
@Transactional
public class QuestionService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private QuestionRepository questionRepository;

   // Managed repository--------------------------------------------------------------------------------

   public QuestionService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Question create() {

	   Question res = new Question();
      return res;
   }

   public Collection<Question> findAll() {
      Collection<Question> res = questionRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Question findOne(int Comment) {
      domain.Question res = questionRepository.findOne(Comment);
      Assert.notNull(res);
      return res;
   }

   public Question save(Question a) {
      Assert.notNull(a);
      Question res = questionRepository.save(a);
      return res;
   }

   public void delete(Question a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      questionRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------

   public void flush() {
	      questionRepository.flush();
	   }
   
}



