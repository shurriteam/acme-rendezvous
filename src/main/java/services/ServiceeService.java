/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;

import domain.Administrator;
import domain.Announcement;
import domain.Category;
import domain.Servicee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AnnouncementRepository;
import repositories.ServiceeRepository;

import java.util.Collection;

@Service
@Transactional
public class ServiceeService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private ServiceeRepository serviceeRepository;
   @Autowired
   private ActorService actorService;

   // Managed repository--------------------------------------------------------------------------------

   public ServiceeService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Servicee create() {

	   Servicee res = new Servicee();
      return res;
   }

   public Collection<Servicee> findAll() {
      Collection<Servicee> res = serviceeRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Servicee findOne(int Comment) {
	  Servicee res = serviceeRepository.findOne(Comment);
      Assert.notNull(res);
      return res;
   }

   public Servicee save(Servicee a) {
      Assert.notNull(a);
      Servicee res = serviceeRepository.save(a);
      return res;
   }

   public void delete(Servicee a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      serviceeRepository.delete(a);
   }
   public void cancel(Servicee s){
      Assert.notNull(s);
      Assert.isTrue(actorService.findByPrincipalHasRol("ADMINISTRATOR"));
      s.setCancelled(true);
      serviceeRepository.save(s);


   }

   // Other business methods -------------------------------------------------------------------------------


   public Collection<Servicee> serviceesByCategory(Category category){
      Assert.notNull(category);
      Collection<Servicee> res = serviceeRepository.serviceesByCategory(category);
      Assert.notNull(category,"Categor�a sin servicios");
      return res;
   }

    public void flush() {
      serviceeRepository.flush();

    }
}



