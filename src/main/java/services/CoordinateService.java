/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package services;


import domain.Coordinate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.CoordinateRepository;

import java.util.Collection;

@Service
@Transactional
public class CoordinateService {

   // Constructors--------------------------------------------------------------------------------------

   @Autowired
   private CoordinateRepository coordinateRepository;

   // Managed repository--------------------------------------------------------------------------------

   public CoordinateService() {
      super();
   }


   // Suporting services --------------------------------------------------------------------------------

   // Simple CRUD method --------------------------------------------------------------------------------

   public Coordinate create() {
      Coordinate res;
      res = new Coordinate();
      return res;
   }

   public Collection<Coordinate> findAll() {
      Collection<Coordinate> res;
      res = coordinateRepository.findAll();
      Assert.notNull(res);
      return res;
   }

   public Coordinate findOne(int Cate) {
      Coordinate res;
      res = coordinateRepository.findOne(Cate);
      Assert.notNull(res);
      return res;
   }

   public Coordinate save(Coordinate a) {
      Assert.notNull(a);
      Coordinate res;
      res = coordinateRepository.save(a);
      return res;

   }

   public void delete(Coordinate a) {
      Assert.notNull(a);
      Assert.isTrue(a.getId() != 0);
      coordinateRepository.delete(a);
   }

   // Other business methods -------------------------------------------------------------------------------

}



