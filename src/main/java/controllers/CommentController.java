/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.Comment;
import domain.Rendezvous;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CommentService;
import services.CoordinateService;
import services.RendezvousService;
import services.UserService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/comment")
public class CommentController extends AbstractController {


   //Services ----------------------------------------------------------------
   @Autowired
   private UserService userService;
   @Autowired
   private CoordinateService coordinateService;
   @Autowired
   private CommentService commentService;
   @Autowired
   private RendezvousService rendezvousService;


   //Constructors----------------------------------------------


   //Create Method -----------------------------------------------------------

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int rendezovousId) {

      ModelAndView result;


      try {
         Comment comment = commentService.create();
         comment.setRndId(rendezovousId);
         result = createEditModelAndView(comment);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());

      }
      return result;
   }

   @RequestMapping(value = "/listAll", method = RequestMethod.GET)
   public ModelAndView comnentList() {

      ModelAndView result;
      Collection<Comment> comment;
      try {
         comment = commentService.findAll();
         result = new ModelAndView("comment/list");
         result.addObject("comment", comment);
         result.addObject("requestURI", "comment/listAll.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView commentList() {

      ModelAndView result;
      Collection<Comment> comment = commentService.findAll();


      try {
         result = new ModelAndView("comment/list");
         result.addObject("comment", comment);
         result.addObject("requestURI", "comment/listAll.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int commentId) {

      ModelAndView result;
      Comment comment;

      try {
         Assert.notNull(commentId);
         comment = commentService.findOne(commentId);
         if (userService.findByPrincipal().getCreatedRendez().contains(comment)) {
            result = createEditModelAndView(comment);
            result.addObject("comment", comment);
         } else {
            throw new IllegalArgumentException("You are not the owner of this comment");
         }

      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Comment comment, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(comment);
      } else {
         try {
            Rendezvous rendezvous = rendezvousService.findOne(comment.getRndId());
            comment.setOwner(userService.findByPrincipal());
            Comment comment1 = commentService.save(comment);
            rendezvous.getComments().add(comment1);
            userService.findByPrincipal().getCreatedComments().add(comment1);
            result = new ModelAndView("administrator/success");
         } catch (Throwable oops) {
            result = createEditModelAndView(comment, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int commentId) {
      ModelAndView result;
      try {
         Assert.notNull(commentId);
         Comment p = commentService.findOne(commentId);
         p.setOwner(null);
         p.setReply(null);
         Collection<Rendezvous> rendezvouses = commentService.rendezvousesWithGivenComment(commentId);
         for (Rendezvous r : rendezvouses) {
            r.getComments().remove(p);
            rendezvousService.save(r);
            rendezvousService.flush();
         }
         Collection<User> users = commentService.usersWithGivenComment(commentId);
         for (User u : users) {
            u.getCreatedComments().remove(p);
            userService.save(u);
            userService.flush();
         }
         commentService.delete(p);
         result = new ModelAndView("redirect:list.do");

      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }


      return result;
   }


   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Comment comment) {
      ModelAndView result;

      result = createEditModelAndView(comment, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Comment comment, String message) {
      ModelAndView result;

      result = new ModelAndView("comment/edit");
      result.addObject("comment", comment);
      result.addObject("message", message);

      return result;

   }


}








