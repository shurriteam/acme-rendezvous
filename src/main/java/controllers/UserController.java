/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.Announcement;
import domain.Form;
import domain.Rendezvous;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.RendezvousService;
import services.UserService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {


   //Services ----------------------------------------------------------------

   @Autowired
   private UserService userService;
   @Autowired
   private RendezvousService rendezvousService;


   //Constructors----------------------------------------------

   public UserController() {
      super();
   }

   protected static ModelAndView createEditModelAndView2(User user) {
      ModelAndView result;

      result = createEditModelAndView2(user, null);

      return result;
   }


   //Create Method -----------------------------------------------------------

//   @RequestMapping(value = "/create", method = RequestMethod.GET)
//   public ModelAndView create() {
//
//      ModelAndView result;
//
//      User user = userService.create();
//      result = createEditModelAndView(user);
//
//      return result;
//
//   }

   protected static ModelAndView createEditModelAndView2(User user, String message) {
      ModelAndView result;

      result = new ModelAndView("user/register");
      result.addObject("user", user);
      result.addObject("message", message);

      return result;

   }

   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView userList() {

      ModelAndView result;
      Collection<User> user;
      user = userService.findAll();
      result = new ModelAndView("user/list");
      result.addObject("user", user);
      result.addObject("requestURI", "user/list.do");

      return result;
   }

   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int userId) {
      ModelAndView result;
      User user;

      user = userService.findOne(userId);
      Assert.notNull(user);
      result = createEditModelAndView(user);
      result.addObject("user", user);


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid User user, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(user);
      } else {
         try {
            userService.save(user);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(user, "property.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int userId) {
      ModelAndView result;
      User p = userService.findOne(userId);

      try {
         userService.delete(p);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         result = createEditModelAndView(p, "user.commit.error");
      }


      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int userId) {
      ModelAndView res;
      User p = userService.findOne(userId);
      res = new ModelAndView("user/view");
      res.addObject("user", p);
      res.addObject("name", p.getName());
      res.addObject("surname", p.getSurname());
      res.addObject("phoneNumber", p.getPhoneNumber());
      res.addObject("email", p.getEmail());
      res.addObject("address", p.getAddress());
      res.addObject("rendezToAssist", p.getCreatedRendez());
      res.addObject("createdRendez", p.getCreatedRendez());

      return res;
   }
   
   @RequestMapping(value = "/viewRendez", method = RequestMethod.GET)
   public ModelAndView viewRendez(@RequestParam int userId, int rendezvousId) {
      ModelAndView res;
      try {
		User p = userService.findOne(userId);
		Rendezvous rendezvous = rendezvousService.findOne(rendezvousId);
		Form form = rendezvousService.formWithGivenUer(userId, rendezvousId);
		boolean formNull = false;
		  res = new ModelAndView("user/view");
		  
		  res.addObject("user", p);
		  res.addObject("name", p.getName());
		  res.addObject("surname", p.getSurname());
		  res.addObject("phoneNumber", p.getPhoneNumber());
		  res.addObject("email", p.getEmail());
		  res.addObject("address", p.getAddress());
         res.addObject("rendezToAssist", p.getCreatedRendez());
         res.addObject("createdRendez", p.getCreatedRendez());
         res.addObject("idR", p.getId());


         if(null!=form){
             res.addObject("renName", rendezvous.getName());
             res.addObject("questions", form.getQuestions());
             res.addObject("answers", form.getAnswers());
             res.addObject("listSize", form.getQuestions().size());
          }else{
             formNull=true;
          }
         res.addObject("notForm" , formNull);

	} catch (Exception e) {
		 res = new ModelAndView("administrator/error");
         res.addObject("trace", e.getMessage());
	}

      return res;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(User user) {
      ModelAndView result;

      result = createEditModelAndView(user, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(User user, String message) {
      ModelAndView result;

      result = new ModelAndView("user/edit");
      result.addObject("user", user);
      result.addObject("message", message);

      return result;

   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create2() {
      ModelAndView result;
      User user = userService.create();
      result = createEditModelAndView2(user);
      return result;
   }
   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
   public ModelAndView register(@Valid User user, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView2(user);
      } else {
         try {
            userService.registerAsUser(user);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndView2(user, "general.commit.error");
         }
      }
      return result;
   }
   
   
   //Announcement list ---------------------
   
   
   @RequestMapping(value = "/listMy", method = RequestMethod.GET)
   public ModelAndView announcementListMy() {

      ModelAndView result;
      Collection<Announcement> announcement;

      try {
    	 User user = userService.findByPrincipal();
         announcement = userService.annoucmentsInbox(user.getId());
         result = new ModelAndView("announcement/list");
         result.addObject("announcements", announcement);
         result.addObject("requestURI", "announcement/listMy.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   
}








