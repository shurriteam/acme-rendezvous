/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import com.sun.org.apache.bcel.internal.generic.LUSHR;
import domain.Answer;
import domain.Form;
import domain.Question;
import domain.Rendezvous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/question")
public class QuestionController extends AbstractController {


   //Services ----------------------------------------------------------------

   @Autowired
   private AnnouncementService announcementService;
   @Autowired
   private UserService userService;
   @Autowired
   private RendezvousService rendezvousService;
   @Autowired
   private QuestionService questionService;
   @Autowired
   private FormService formService;
   @Autowired
   private AnswerService answerService;


   //Constructors----------------------------------------------


   //Create Method -----------------------------------------------------------


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;


      try {
         Collection<Rendezvous> rendezvouses = userService.findByPrincipal().getCreatedRendez();
         Question question = questionService.create();
         result = createEditModelAndView(question);
         result.addObject("question", question);
         result.addObject("rendezvouses", rendezvouses);

      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;
   }


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView list(@RequestParam int rendezvousId) {

      ModelAndView result;
      Collection<Question> questions;

      try {
    	  Rendezvous aux = rendezvousService.findOne(rendezvousId);
    	  if(userService.findByPrincipal().getCreatedRendez().contains(aux)) {
    		  questions = aux.getQuestions();
    	         result = new ModelAndView("question/list");
//    	         result.addObject("idRe", rendezvousId);
    	         result.addObject("questions", questions);
    	         result.addObject("requestURI", "question/list.do");
    	  }else {
    		  throw new UnsupportedOperationException("This rendezvous is not your.");
    	  }
    	
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int questionId) {

      ModelAndView result;
      Question question;

      try {
         Assert.notNull(questionId);
         question = questionService.findOne(questionId);

            result = createEditModelAndView(question);
            result.addObject("question", question);

      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Question question, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(question);

      } else {
         try {

            Question question1 = questionService.save(question);
            question1.getRendezvous().getQuestions().add(question1);
            rendezvousService.save(question1.getRendezvous());
            result = new ModelAndView("administrator/success");
         } catch (Throwable oops) {
            result = createEditModelAndView(question, "general.commit.error");
         }
      }
      return result;
   }


   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int questionId) {
      ModelAndView result;


      try {
         Assert.notNull(questionId);
         Question p = questionService.findOne(questionId);
        	 questionService.delete(p);
            result = new ModelAndView("redirect:list.do");

      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }


      return result;
   }


   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Question question) {
      ModelAndView result;

      result = createEditModelAndView(question, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Question question, String message) {
      ModelAndView result;

      result = new ModelAndView("question/edit");
      result.addObject("question", question);
      result.addObject("message", message);

      return result;

   }


   protected ModelAndView createEditModelAndView2(Form form) {
      ModelAndView result;

      result = createEditModelAndView2(form, null);

      return result;
   }

   protected ModelAndView createEditModelAndView2(Form form, String message) {
      ModelAndView result;

      result = new ModelAndView("question/answers");
      result.addObject("form", form);
      result.addObject("message", message);

      return result;

   }


   @RequestMapping(value = "/answer", method = RequestMethod.GET)
   public ModelAndView answer(@RequestParam int rendezvousId) {

      ModelAndView result;
      try {
         Rendezvous rendezvous = rendezvousService.findOne(rendezvousId);

         if(rendezvous.getQuestions().isEmpty() || null==rendezvous.getQuestions()){

            rendezvous.getAssistants().add(userService.findByPrincipal());
            userService.findByPrincipal().getRendezToAssist().add(rendezvous);
            result = new ModelAndView("administrator/success");

         }else{
            Form form = formService.create();
            form.setQuestions(rendezvous.getQuestions());
            form.setAnswers(new ArrayList<Answer>(rendezvous.getQuestions().size()));
            for (Question q : form.getQuestions()){
               Answer answer =answerService.create();
               answer.setText("_");
               Answer answer1 = answerService.save(answer);
               form.getAnswers().add(answer1);
            }
            form.setOwner(userService.findByPrincipal());
            form.setRendezvous(rendezvous);
            formService.save(form);
            result = createEditModelAndView2(form);
            result.addObject("listSize", rendezvous.getQuestions().size());
         }



      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }
      return result;
   }


   @RequestMapping(value = "/answer", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@ModelAttribute(value = "form") @Valid Form form, BindingResult binding, HttpServletRequest req) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView2(form);

      } else {
         try {
            form.setOwner(userService.findByPrincipal());


            String[] anwers = req.getParameterValues("answers");
            String[] splitted = anwers[0].split(",");
            for (int n = 0; n <= splitted.length - 1; n++) {
              Answer answer = answerService.findOne(new Integer(splitted[n]));
              answerService.save(answer);

            }
            form.getRendezvous().getForms().add(form);

            form.getRendezvous().getAssistants().add(form.getOwner());
            form.getOwner().getRendezToAssist().add(form.getRendezvous());
            userService.save(form.getOwner());
            rendezvousService.save(form.getRendezvous());

            result = new ModelAndView("administrator/success");
         } catch (Throwable oops) {
            result = createEditModelAndView2(form, "general.commit.error");
         }
      }
      return result;
   }



   @RequestMapping(value = "cancelRNDZ.do", method = RequestMethod.GET)
   public ModelAndView cancnelRNDZ(@RequestParam int rendezvousId) {
      ModelAndView result;

         try {
            Rendezvous rendezvous = rendezvousService.findOne(rendezvousId);
            if(rendezvous.getAssistants().contains(userService.findByPrincipal())){
               rendezvous.getAssistants().remove(userService.findByPrincipal());
               userService.findByPrincipal().getRendezToAssist().remove(rendezvous);
               result = new ModelAndView("administrator/success");
            }else{
               throw new NoSuchElementException("No se encuentra el usuario en este rendezvous");
            }

         } catch (Throwable oops) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", oops.getMessage());
         }

      return result;
   }

}








