/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.Manager;
import domain.Servicee;
import domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ManagerService;
import services.RendezvousService;
import services.ServiceeService;
import services.UserService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/managr")
public class ManagerController extends AbstractController {


   //Services ----------------------------------------------------------------

   @Autowired
   private ManagerService managerService;
   @Autowired
   private UserService userService;
   @Autowired
   private RendezvousService rendezvousService;
   @Autowired
   private ServiceeService serviceeService;


   //Constructors----------------------------------------------


   // Create Method -----------------------------------------------------------

   /*@RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;


      try {
         Manager manager = managerService.create();


         result = createEditModelAndView(manager);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());

      }
      return result;
   }



  /* @RequestMapping(value = "/create1", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int rendezvousId) {

      ModelAndView result;


      try {
         Assert.notNull(rendezvousId);
         Manager manager = managerService.create();

//         managerService.save(manager);
         result = createEditModelAndView(manager);
         result.addObject("manager", manager);
//            result.addObject("manager", manager);
//         if(userService.findByPrincipal().getCreatedRendez().contains(manager)){
//            result = createEditModelAndView(manager);
//            result.addObject("manager", manager);
//         }else {
//            throw new IllegalArgumentException("You are not the owner of this manager");
//         }

      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;
   }*/

   @RequestMapping(value = "/listServices", method = RequestMethod.GET)
   public ModelAndView rendezvousListAll () {

      ModelAndView result;
      Collection<Servicee> servicees;

      try {
         servicees = serviceeService.findAll();
         result = new ModelAndView("manager/listServices");
         result.addObject("serviceesAll", servicees);
         result.addObject("servicees", managerService.findByPrincipal().getServices());
         result.addObject("requestURI", "servicee/listAll.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/listAll", method = RequestMethod.GET)
   public ModelAndView managerListAll() {

      ModelAndView result;
      Collection<Manager> manager;

      try {
         manager = managerService.findAll();
         result = new ModelAndView("manager/list");
         result.addObject("managers", manager);
         result.addObject("requestURI", "manager/listAll.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }


   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int managerId) {

      ModelAndView result;
      Manager manager;

      try {
         Assert.notNull(managerId);
         manager = managerService.findOne(managerId);
         if (userService.findByPrincipal().getCreatedRendez().contains(manager)) {
            result = createEditModelAndView(manager);
            result.addObject("manager", manager);
         } else {
            throw new IllegalArgumentException("You are not the owner of this manager");
         }

      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Manager manager, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(manager);

      } else {
         try {
            managerService.save(manager);
            result = new ModelAndView("redirect:/rendezvous/list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(manager, "general.commit.error");
         }
      }
      return result;
   }


   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
   public ModelAndView register(@Valid Manager manager, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView2(manager);
      } else {
         try {
            managerService.registerAsManager(manager);
            result = new ModelAndView("welcome/index");
         } catch (Throwable oops) {
            result = createEditModelAndView2(manager, "general.commit.error");
         }
      }
      return result;
   }


   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int managerId) {
      ModelAndView result;


      try {
         Assert.notNull(managerId);
         Manager p = managerService.findOne(managerId);
         managerService.delete(p);
         result = new ModelAndView("redirect:listAll.do");

      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }


      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int managerId) {
      ModelAndView res;
      try {
         Manager p = managerService.findOne(managerId);
         res = new ModelAndView("manager/view");

         res.addObject("requestURI", "manager/view.do");
      } catch (Exception e) {
         res = new ModelAndView("administrator/error");
         res.addObject("trace", e.getMessage());
      }
      return res;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Manager manager) {
      ModelAndView result;

      result = createEditModelAndView(manager, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Manager manager, String message) {
      ModelAndView result;

      result = new ModelAndView("manager/edit");
      result.addObject("manager", manager);
      result.addObject("message", message);

      return result;

   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create2() {
      ModelAndView result;
      Manager manager = managerService.create();
      result = createEditModelAndView2(manager);
      return result;
   }

   // Ancillary methods2 ------------------------------------------------


   protected ModelAndView createEditModelAndView2(Manager manager) {
      ModelAndView result;

      result = createEditModelAndView2(manager, null);

      return result;
   }

   protected ModelAndView createEditModelAndView2(Manager manager, String message) {
      ModelAndView result;

      result = new ModelAndView("manager/register");
      result.addObject("manager", manager);
      result.addObject("message", message);

      return result;

   }


}








