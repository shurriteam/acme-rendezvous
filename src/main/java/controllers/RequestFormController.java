package controllers;


import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;
import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/requestForm")
public class RequestFormController {

    @Autowired
    private RequestFormService requestFormService;
    @Autowired
    private ServiceeService serviceeService;
    @Autowired
    private UserService userService;
    @Autowired
    private RendezvousService rendezvousService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private CreditCardService creditCardService;



    @RequestMapping(value = "/createForm", method = RequestMethod.GET)
    public ModelAndView createRequestForm (@RequestParam int serviceeId) {

        ModelAndView result;
        Servicee servicees;
        Collection<Rendezvous> rendezvous;
       if (creditCardService.checkCreditCard(userService.findByPrincipal().getCreditCard()).equals(true)) {
        try {
            RequestForm requestForm = requestFormService.create();
            servicees = serviceeService.findOne(serviceeId);
            User user=userService.findByPrincipal();
            requestForm.setServicee(servicees);

            result = new ModelAndView("servicee/requestForm");
            result.addObject("userRendezvous", user.getCreatedRendez());
            result.addObject("requestForm", requestForm);

        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }
       } else {
          result = new ModelAndView("credit-card/error");
       }
        return result;
    }



    @RequestMapping(value = "/edit", method = RequestMethod.POST,params = "request")
    public ModelAndView edit(@Valid RequestForm requestForm, BindingResult bindingResult){
        ModelAndView result;
        if(bindingResult.hasErrors())
            result = createEditModelAndView(requestForm);
        else{
            try{
                requestForm.getServicee().getRendezvous().add(requestForm.getRendezvous());
                requestForm.getRendezvous().getServicees().add(requestForm.getServicee());
                requestFormService.save(requestForm);


                result = new ModelAndView("servicee/list");
                result.addObject("servicees", serviceeService.findAll());
                result.addObject("requestURI", "servicee/listAll.do");
               result.addObject("message", "general.commit.ok");

            }catch (Throwable oops){
                result = createEditModelAndView(requestForm,"request.save.error");
            }
        }
        return result;
    }


    // Ancillary methods ------------------------------------------------

    protected ModelAndView createEditModelAndView(RequestForm requestForm) {
        ModelAndView result;


        result = createEditModelAndView(requestForm, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(RequestForm requestForm, String message) {
        ModelAndView result;


        User user=userService.findByPrincipal();


        result = new ModelAndView("servicee/requestForm");
        result.addObject("requestForm", requestForm);
        result.addObject("message", message);
        result.addObject("userRendezvous", user.getCreatedRendez());


        return result;

    }
}
