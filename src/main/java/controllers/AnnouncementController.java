/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.Announcement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.AnnouncementService;
import services.RendezvousService;
import services.UserService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping("/announcement")
public class AnnouncementController extends AbstractController {


   //Services ----------------------------------------------------------------

   @Autowired
   private AnnouncementService announcementService;
   @Autowired
   private UserService userService;
   @Autowired
   private RendezvousService rendezvousService;


   //Constructors----------------------------------------------


   //Create Method -----------------------------------------------------------
//
//   @RequestMapping(value = "/create", method = RequestMethod.GET)
//   public ModelAndView create() {
//
//      ModelAndView result;
//
//
//      try {
//         Announcement announcement = announcementService.create();
//         announcement.setCreationMomment(new Date(System.currentTimeMillis()-1000));
//
//
//
//         result = createEditModelAndView(announcement);
//      } catch (Exception e) {
//         result = new ModelAndView("administrator/error");
//         result.addObject("trace", e.getMessage());
//
//      }
//      return result;
//   }


   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create(@RequestParam int rendezvousId) {

      ModelAndView result;


      try {
         Assert.notNull(rendezvousId);
         Announcement announcement = announcementService.create();
         announcement.setRendezvous(rendezvousService.findOne(rendezvousId));

//         announcementService.save(announcement);
         result = createEditModelAndView(announcement);
         result.addObject("announcement", announcement);
//            result.addObject("announcement", announcement);
//         if(userService.findByPrincipal().getCreatedRendez().contains(announcement)){
//            result = createEditModelAndView(announcement);
//            result.addObject("announcement", announcement);
//         }else {
//            throw new IllegalArgumentException("You are not the owner of this announcement");
//         }

      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;
   }

   @RequestMapping(value = "/listAll", method = RequestMethod.GET)
   public ModelAndView announcementListAll() {

      ModelAndView result;
      Collection<Announcement> announcement;

      try {
         announcement = announcementService.findAll();
         result = new ModelAndView("announcement/list");
         result.addObject("announcements", announcement);
         result.addObject("requestURI", "announcement/listAll.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/listMyCreated", method = RequestMethod.GET)
   public ModelAndView announcementList() {

      ModelAndView result;
      Collection<Announcement> announcement;
      announcement = userService.createdAnnouncementsByUser(userService.findByPrincipal().getId());

      try {

         result = new ModelAndView("announcement/list");
         result.addObject("announcements", announcement);
         result.addObject("requestURI", "announcement/listMyCreated.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int announcementId) {

      ModelAndView result;
      Announcement announcement;

      try {
         Assert.notNull(announcementId);
         announcement = announcementService.findOne(announcementId);
         if (userService.findByPrincipal().getCreatedRendez().contains(announcement)) {
            result = createEditModelAndView(announcement);
            result.addObject("announcement", announcement);
         } else {
            throw new IllegalArgumentException("You are not the owner of this announcement");
         }

      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Announcement announcement, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndView(announcement);

      } else {
         try {
            announcement.setCreationMomment(new Date(System.currentTimeMillis() - 100));
            announcementService.save(announcement);
            rendezvousService.save(announcement.getRendezvous());

            result = new ModelAndView("redirect:/rendezvous/list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(announcement, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int announcementId) {
      ModelAndView result;


      try {
         Assert.notNull(announcementId);
         Announcement p = announcementService.findOne(announcementId);
            announcementService.delete(p);
         result = new ModelAndView("redirect:listAll.do");

      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }


      return result;
   }

   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int announcementId) {
      ModelAndView res;
      try {
         Announcement p = announcementService.findOne(announcementId);
         res = new ModelAndView("announcement/view");

         res.addObject("requestURI", "announcement/view.do");
      } catch (Exception e) {
         res = new ModelAndView("administrator/error");
         res.addObject("trace", e.getMessage());
      }
      return res;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Announcement announcement) {
      ModelAndView result;

      result = createEditModelAndView(announcement, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Announcement announcement, String message) {
      ModelAndView result;

      result = new ModelAndView("announcement/edit");
      result.addObject("announcement", announcement);
      result.addObject("message", message);

      return result;

   }


}








