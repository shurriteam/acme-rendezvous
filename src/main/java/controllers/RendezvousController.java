/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import security.Authority;
import services.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

@Controller
@RequestMapping("/rendezvous")
public class RendezvousController extends AbstractController {


   //Services ----------------------------------------------------------------

   @Autowired
   private RendezvousService rendezvousService;
   @Autowired
   private UserService userService;
   @Autowired
   private CoordinateService coordinateService;
   @Autowired
   private AnnouncementService announcementService;
   @Autowired
   private QuestionService questionService;
   @Autowired
   private FormService formService;
   @Autowired
   private AdministratorService administratorService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private ServiceeService serviceeService;
   @Autowired
   private CategoryService categoryService;


   //Constructors----------------------------------------------



   //Create Method -----------------------------------------------------------

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {
      ModelAndView result;


      try {
         Rendezvous rendezvous = rendezvousService.create();
         rendezvous.setRendezStatus(RendezStatus.DRAFT);
         result = createEditModelAndView(rendezvous);
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());

      }
      return result;
   }

   @RequestMapping(value = "/listAll", method = RequestMethod.GET)
   public ModelAndView rendezvousListAll() {

      ModelAndView result;
      Collection<Rendezvous> rendezvous;

      try {
         rendezvous = rendezvousService.findAll();
         result = new ModelAndView("rendezvous/list");
         result.addObject("rendezvouses", rendezvous);
         result.addObject("requestURI", "rendezvous/listAll.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/listNotMy", method = RequestMethod.GET)
   public ModelAndView rendezvousListNotMy() {

      ModelAndView result;
      Collection<Rendezvous> rendezvous;

      if (userService.findByPrincipal().getAge() >= 18) {
         try {
            rendezvous = rendezvousService.findAll();
            Collection<Rendezvous> my = userService.findByPrincipal().getCreatedRendez();
            rendezvous.removeAll(my);
            result = new ModelAndView("rendezvous/list");
            result.addObject("rendezvouses", rendezvous);
            result.addObject("requestURI", "rendezvous/listNotMy.do");
         } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
         }

         return result;
      } else {
         try {
            rendezvous = rendezvousService.findAll();
            Collection<Rendezvous> adults = rendezvousService.rendezvousesForAdults();
            rendezvous.removeAll(adults);
            Collection<Rendezvous> my = userService.findByPrincipal().getCreatedRendez();
            rendezvous.removeAll(my);
            result = new ModelAndView("rendezvous/list");
            result.addObject("rendezvouses", rendezvous);
            result.addObject("requestURI", "rendezvous/listNotMy.do");
         } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
         }

         return result;

      }
   }

   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView rendezvousList() {

      ModelAndView result;
      Collection<Rendezvous> rendezvous;
      Boolean my = true;

      try {
         User logedUser = userService.findByPrincipal();
         rendezvous = userService.createdRenezByUser(logedUser.getId());
         result = new ModelAndView("rendezvous/list");
         result.addObject("rendezvouses", rendezvous);
         result.addObject("my", my);
         result.addObject("requestURI", "rendezvous/listAll.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/listToAssist", method = RequestMethod.GET)
   public ModelAndView rendezvousListToAssist() {

      ModelAndView result;
      Collection<Rendezvous> rendezvous;
      Boolean my = true;

      try {
         User logedUser = userService.findByPrincipal();
         rendezvous = logedUser.getRendezToAssist();
         result = new ModelAndView("rendezvous/list");
         result.addObject("rendezvouses", rendezvous);
         result.addObject("my", my);
         result.addObject("requestURI", "rendezvous/listToAssist.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }
   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int rendezvousId) {

      ModelAndView result;
      Rendezvous rendezvous;

      try {
         Assert.notNull(rendezvousId);
         rendezvous = rendezvousService.findOne(rendezvousId);
         if(userService.findByPrincipal().getCreatedRendez().contains(rendezvous) && rendezvous.getRendezStatus().equals(RendezStatus.DRAFT)){
            result = createEditModelAndView(rendezvous);
            result.addObject("rendezvous", rendezvous);
         }else {
            throw new IllegalArgumentException("You are not the owner of this rendezvous or it is in FINAL mode");
         }

      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Rendezvous rendezvous, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(rendezvous);
      } else {
         try {
            Coordinate aux = coordinateService.save(rendezvous.getLocalization());
            userService.flush();
            rendezvous.getAssistants().add(userService.findByPrincipal());
            rendezvous.setOwner(userService.findByPrincipal());

            rendezvous.setLocalization(aux);
            rendezvous.setOrganizedMomment(new Date(System.currentTimeMillis() - 100));
            Rendezvous rendezvous1 = rendezvousService.save(rendezvous);
            rendezvousService.flush();
            userService.findByPrincipal().getCreatedRendez().add(rendezvous1);
            rendezvousService.save(rendezvous1);

            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(rendezvous, "general.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int rendezvousId) {
      ModelAndView result;


      try {
            Actor actor = actorService.findByPrincipal();
            Assert.notNull(rendezvousId);
            Rendezvous p = rendezvousService.findOne(rendezvousId);
            Authority authority = new Authority();
            authority.setAuthority("USER");

            if(actor.getUserAccount().getAuthorities().contains(authority)){

               if (p.getRendezStatus().equals(RendezStatus.DRAFT) && userService.findByPrincipal().getCreatedRendez().contains(p)) {
                  p.setLocalization(null);

                  p.setAnnouncements(null);
                  p.setAssistants(null);

                  Collection<Announcement> announcements = rendezvousService.announcementsGivenARendez(rendezvousId);
                  for (Announcement u : announcements) {
                     u.setRendezvous(null);
                     announcementService.save(u);
                  }
                  Collection<Rendezvous> related = rendezvousService.relatedRendezsGivenARendez(rendezvousId);
                  for (Rendezvous r : related) {
                     r.getRelated().remove(p);
                     rendezvousService.save(r);
                  }

                  Collection<Servicee> servicees = rendezvousService.relatedRendezsGivenAServicee(rendezvousId);
                  for (Servicee r : servicees) {
                     r.getRendezvous().remove(p);
                     serviceeService.save(r);
                  }

                  p.setRelated(new ArrayList<Rendezvous>());
                  rendezvousService.save(p);
                  p.setAnnouncements(null);
                  p.setComments(null);

                  p.setForms(null);
                  User u = p.getOwner();
                  u.getCreatedRendez().remove(p);
                  userService.save(u);
                  p.setOwner(null);
//         for (Question q : rendezvousService.relatedQuestionsGivenARendez(rendezvousId)) {
//            q.setRendezvous(null);
//            questionService.delete(q);
//            // q.setRendezvous(null);
//            // questionService.save(q);
//         }
                  for (Form form : rendezvousService.relatedFormsGivenARendez(rendezvousId)) {
                     form.setRendezvous(null);
                     form.setOwner(null);
                     form.setAnswers(null);
                     form.setQuestions(new HashSet<Question>());
                     formService.delete(form);
                  }
                  p.setQuestions(null);
                  p.setForms(null);
//         p.setQuestions(null);
                  rendezvousService.delete(p);
                  result = new ModelAndView("redirect:listAll.do");
               } else {
                  throw new IllegalArgumentException("You are not the owner of this rendezvous or it is in FINAL mode");
               }
            }else {
               p.setLocalization(null);

               p.setAnnouncements(null);
               p.setAssistants(null);

               Collection<Announcement> announcements = rendezvousService.announcementsGivenARendez(rendezvousId);
               for (Announcement u : announcements) {
                  u.setRendezvous(null);
                  announcementService.save(u);
               }
               Collection<Rendezvous> related = rendezvousService.relatedRendezsGivenARendez(rendezvousId);
               for (Rendezvous r : related) {
                  r.getRelated().remove(p);
                  rendezvousService.save(r);
               }
               p.setRelated(new ArrayList<Rendezvous>());
               rendezvousService.save(p);
               p.setAnnouncements(null);
               p.setComments(null);

               p.setForms(null);
               User u = p.getOwner();
               u.getCreatedRendez().remove(p);
               userService.save(u);
               p.setOwner(null);
//         for (Question q : rendezvousService.relatedQuestionsGivenARendez(rendezvousId)) {
//            q.setRendezvous(null);
//            questionService.delete(q);
//            // q.setRendezvous(null);
//            // questionService.save(q);
//         }
               for (Form form : rendezvousService.relatedFormsGivenARendez(rendezvousId)) {
                  form.setRendezvous(null);
                  form.setOwner(null);
                  form.setAnswers(null);
                  form.setQuestions(new HashSet<Question>());
                  formService.delete(form);
               }
               p.setQuestions(null);
               p.setForms(null);
//         p.setQuestions(null);
               rendezvousService.delete(p);
               result = new ModelAndView("redirect:listAll.do");
            }



      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }


      return result;
   }



   @RequestMapping(value = "/view", method = RequestMethod.GET)
   public ModelAndView view(@RequestParam int rendezvousId) {
      ModelAndView res;
      Boolean authenticatedStatus = false;
      Boolean isMy = false;
      if (userService.findByPrincipal().getAge() >= 18) {
         try {
            res = new ModelAndView("rendezvous/view");
            Rendezvous p = rendezvousService.findOne(rendezvousId);
            User authenticated = userService.findByPrincipal();
            if (authenticated.getRendezToAssist().contains(p)) {
               authenticatedStatus = true;
            } else if (authenticated.getCreatedRendez().contains(p)) {
               isMy = true;
            }
            res.addObject("servicees", p.getServicees());
            res.addObject("idR", p.getId());
            res.addObject("name", p.getName());
            res.addObject("description", p.getDescription());
            res.addObject("localization", p.getLocalization());
            res.addObject("owner", p.getOwner());
            res.addObject("organizedMomment", p.getOrganizedMomment());
            res.addObject("comments", p.getComments());
            res.addObject("picture", p.getPicture());
            res.addObject("related", p.getRelated());
            res.addObject("assistants", p.getAssistants());
            res.addObject("announcements", p.getAnnouncements());
            res.addObject("auth", authenticatedStatus);
            res.addObject("requestURI", "rendezvous/view.do");
            res.addObject("isMy", isMy);

         } catch (Exception e) {
            res = new ModelAndView("administrator/error");
            res.addObject("trace", e.getMessage());
         }
         return res;
      } else {
         try {
            res = new ModelAndView("rendezvous/view");
            Rendezvous p = rendezvousService.findOne(rendezvousId);
            User authenticated = userService.findByPrincipal();
            if (authenticated.getRendezToAssist().contains(p)) {
               authenticatedStatus = true;
            } else if (authenticated.getCreatedRendez().contains(p)) {
               isMy = true;
            }
            Collection<Rendezvous> adults = rendezvousService.rendezvousesForAdults();
            Collection<Rendezvous> toShow = new ArrayList<>(p.getRelated());
            toShow.removeAll(adults);
            res.addObject("idR", p.getId());
            res.addObject("name", p.getName());
            res.addObject("description", p.getDescription());
            res.addObject("localization", p.getLocalization());
            res.addObject("owner", p.getOwner());
            res.addObject("organizedMomment", p.getOrganizedMomment());
            res.addObject("comments", p.getComments());
            res.addObject("picture", p.getPicture());
            res.addObject("related", toShow);
            res.addObject("assistants", p.getAssistants());
            res.addObject("announcements", p.getAnnouncements());
            res.addObject("auth", authenticatedStatus);
            res.addObject("requestURI", "rendezvous/view.do");
            res.addObject("isMy", isMy);

         } catch (Exception e) {
            res = new ModelAndView("administrator/error");
            res.addObject("trace", e.getMessage());
         }
         return res;
      }
   }


   @RequestMapping(value = "/viewNotAuthenticated", method = RequestMethod.GET)
   public ModelAndView viewNotAuthenticated(@RequestParam int rendezvousId) {
      ModelAndView res;
      try {
         Rendezvous p = rendezvousService.findOne(rendezvousId);
         res = new ModelAndView("rendezvous/view");
         res.addObject("servicees", p.getServicees());
         res.addObject("name", p.getName());
         res.addObject("description", p.getDescription());
         res.addObject("localization", p.getLocalization());
         res.addObject("owner", p.getOwner());
         res.addObject("organizedMomment", p.getOrganizedMomment());
         res.addObject("comments", p.getComments());
         res.addObject("picture", p.getPicture());
         res.addObject("idR", p.getId());
         res.addObject("related", p.getRelated());
         res.addObject("assistants", p.getAssistants());
         res.addObject("announcements", p.getAnnouncements());
         res.addObject("requestURI", "rendezvous/view.do");
      } catch (Exception e) {
         res = new ModelAndView("administrator/error");
         res.addObject("trace", e.getMessage());
      }
      return res;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Rendezvous rendezvous) {
      ModelAndView result;

      result = createEditModelAndView(rendezvous, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Rendezvous rendezvous, String message) {
      ModelAndView result;

      result = new ModelAndView("rendezvous/edit");
      result.addObject("rendezvous", rendezvous);
      result.addObject("message", message);

      return result;

   }

   @RequestMapping(value = "/associate", method = RequestMethod.GET)
   public ModelAndView associate(@RequestParam int rendezvousId) {

      ModelAndView res;
      try {
         Rendezvous p = rendezvousService.findOne(rendezvousId);
         res = new ModelAndView("rendezvous/associate");
         res.addObject("rendezvousesTo", rendezvousService.findAll());
         res.addObject("rendezvous", p);
         res.addObject("requestURI", "rendezvous/associate.do");
      } catch (Exception e) {
         res = new ModelAndView("administrator/error");
         res.addObject("trace", e.getMessage());
      }
      return res;

   }

   @RequestMapping(value = "/associate", method = RequestMethod.POST, params = "save")
   public ModelAndView associate(@Valid Rendezvous rendezvous, BindingResult binding, HttpServletRequest request) {
      ModelAndView result;

      if (!binding.hasErrors()) {
         result = createEditModelAndView(rendezvous);
      } else {
         try {
            String[] selectedStudentIds = request.getParameterValues("rendezvous");
            Collection<Rendezvous> rendezTo = new HashSet<>();
            for (int n = 0; n <= selectedStudentIds.length - 1; n++) {
               Rendezvous rendezvous1 = rendezvousService.findOne(new Integer(selectedStudentIds[n]));
               rendezTo.add(rendezvous1);
            }


//              Set<Rendezvous> related = new HashSet<>(rendezvous.getRelated());
//               related.addAll(rendezTo);
            rendezvous.getRelated().addAll(rendezTo);
            rendezvousService.save(rendezvous);

//            selectedStudentIds = binding.get("rendezvous");
//            rendezvousService.save(rendezvous1);

            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(rendezvous, "general.commit.error");
         }
      }
      return result;
   }


   //PUBLISH RENDEZVOUS


   @RequestMapping(value = "/publish", method = RequestMethod.GET)
   public ModelAndView publish(@RequestParam int rendezvousId) {

      ModelAndView result;
      Rendezvous rendezvous;

      try {
         Assert.notNull(rendezvousId);
         rendezvous = rendezvousService.findOne(rendezvousId);
         if(userService.findByPrincipal().getCreatedRendez().contains(rendezvous) && rendezvous.getRendezStatus().equals(RendezStatus.DRAFT)){
            rendezvous.setRendezStatus(RendezStatus.FINAL);
            rendezvousService.save(rendezvous);
            result = new ModelAndView("administrator/success");
         }else {
            throw new IllegalArgumentException("it is in FINAL mode");
         }

      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;
   }





}








