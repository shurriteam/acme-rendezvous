/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;

import domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.CategoryService;

import javax.validation.Valid;
import java.util.Collection;


@Controller
@RequestMapping("/category")
public class CategoryController extends AbstractController {

   @Autowired
   private CategoryService categoryService;


   public CategoryController() {
      super();
   }

   @RequestMapping(value = "/list")
   public ModelAndView list() {
      ModelAndView result;
      Collection<Category> aux = categoryService.findAll();
      result = new ModelAndView("category/list");
      result.addObject("categories", aux);
      result.addObject("father", categoryService.findAll());
      result.addObject("sons", categoryService.findAll());
      result.addObject("requestURI", "category/list.do");
      return result;


   }


   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int categoryId) {
      ModelAndView result;
      Category categorie = categoryService.findOne(categoryId);
      Assert.notNull(categorie);
      result = createEditModelAndView(categorie);
      result.addObject("categoriezz", categoryService.findAll());
//      result.addObject("father", categoryService.findAll());
//      result.addObject("sons", categoryService.findAll());
      return result;
   }

//   @RequestMapping(value = "/view", method = RequestMethod.GET)
//   public ModelAndView view(@RequestParam int categoryId) {
//
//      ModelAndView result;
//      try {
//         Category categorie = categoryService.findOne(categoryId);
//         Assert.notNull(categorie);
//         result = new ModelAndView("category/view");
//         result.addObject("trips", categorie.getAssociatedServices());
//         result.addObject("name", categorie.getName());
//         result.addObject("sons", categorie.getSons());
//         result.addObject("id", categorie.getId());
//         result.addObject("requestURI", "category/view.do");
//         return result;
//      } catch (Exception e) {
//         result = new ModelAndView("administrator/error");
//         result.addObject("trace", e.getMessage());
//      }
//
//      return result;
//   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Category categorie, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(categorie);
      } else {
         try {

            categoryService.save(categorie);
            result = new ModelAndView("redirect:list.do");

         } catch (Throwable oops) {
            result = createEditModelAndView(categorie, "comment.commit.error");
            result.addObject("sons", categoryService.findAll());
            result.addObject("categoriezz", categoryService.findAll());
         }
      }

      return result;
   }

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {
      ModelAndView r;
      try {
         Category category = categoryService.create();
         r = createEditModelAndView(category);
         r.addObject("categoriezz", categoryService.findAll());
      } catch (Exception e) {
         r = new ModelAndView("administrator/error");
         r.addObject("trace", e.getMessage());
      }
      return r;
   }

   @RequestMapping(value = "/delete", method = RequestMethod.GET)
   public ModelAndView delete(@RequestParam int categoryId) {
      ModelAndView result;
      try {

      Category category = categoryService.findOne(categoryId);
         if (category.equals(categoryService.getCategoryUncategorize())){
            throw new UnsupportedOperationException("This category cannot be deleted");
         }else {
            if(!category.getAssociatedServices().isEmpty()){
               categoryService.uncategorizedServices(category);
            }
            if(!category.getSons().isEmpty()){
            }

            categoryService.delete(category);
            categoryService.flush();
            result = new ModelAndView("redirect: list.do");
         }



      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }





   protected ModelAndView createEditModelAndView(Category categorie) {
      ModelAndView result;

      result = createEditModelAndView(categorie, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Category categorie, String message) {
      ModelAndView result;
      result = new ModelAndView("category/edit");
      result.addObject("category", categorie);


      return result;


   }



   @RequestMapping(value = "/listService", method = RequestMethod.GET)
   public ModelAndView rendezvousListCategory() {

      ModelAndView result;
      Collection<Category> categories;


      try {
         categories = categoryService.findAll();
         result = new ModelAndView("rendezvous/listCat");
         result.addObject("categories", categories);
         result.addObject("requestURI", "rendezvous/listAll.do");
      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }

      return result;
   }
}
