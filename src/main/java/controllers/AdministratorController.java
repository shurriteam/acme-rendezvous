/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package controllers;


import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

   //Services ----------------------------------------------------------------


   @Autowired
   private AdministratorService administratorService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private CommentService commentService;
   @Autowired
   private RendezvousService rendezvousService;
   @Autowired
   public AnnouncementService announcementService;
   @Autowired
   public ServiceeService serviceeService;
   @Autowired
   private BussinesNameService bussinesNameService;
   @Autowired
   private WelcomePhraseService welcomePhraseService;

   //Constructors----------------------------------------------

   public AdministratorController() {
      super();
   }


   @RequestMapping(value = "/list", method = RequestMethod.GET)
   public ModelAndView administratorList() {

      ModelAndView result;
      Collection<Administrator> administrators;

      administrators = administratorService.findAll();
      result = new ModelAndView("administrator/list");
      result.addObject("administrators", administrators);
      result.addObject("requestURI", "administrator/list.do");

      return result;
   }




   //Create Method -----------------------------------------------------------

   @RequestMapping(value = "/create", method = RequestMethod.GET)
   public ModelAndView create() {

      ModelAndView result;

      Administrator administrator = administratorService.create();
      result = createEditModelAndView(administrator);

      return result;

   }

   // Edition ---------------------------------------------------------

   @RequestMapping(value = "/edit", method = RequestMethod.GET)
   public ModelAndView edit(@RequestParam int administratorId) {
      ModelAndView result;
      Administrator administrator;

      administrator = administratorService.findOne(administratorId);
      Assert.notNull(administrator);
      result = createEditModelAndView(administrator);

      return result;
   }


   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
   public ModelAndView save(@Valid Administrator administrator, BindingResult binding) {
      ModelAndView result;

      if (binding.hasErrors()) {
         result = createEditModelAndView(administrator);
      } else {
         try {
            administratorService.save(administrator);
            result = new ModelAndView("redirect:list.do");
         } catch (Throwable oops) {
            result = createEditModelAndView(administrator, "administrator.commit.error");
         }
      }
      return result;
   }

   @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
   public ModelAndView delete(Administrator administrator) {
      ModelAndView result;
      try {

         administratorService.delete(administrator);
         result = new ModelAndView("redirect:list.do");
      } catch (Throwable oops) {
         result = createEditModelAndView(administrator, "administrator.commit.error");
      }

      return result;
   }

   @RequestMapping(value = "/cancelService", method = RequestMethod.GET)
   public ModelAndView cancel2(@RequestParam int serviceeId) {
      ModelAndView result;
      try {

         Servicee servicee = serviceeService.findOne(serviceeId);

         if(!servicee.isCancelled()){
            servicee.setCancelled(true);
            serviceeService.save(servicee);
         }
         result = new  ModelAndView("redirect:../servicee/listAll.do");
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }

      return result;
   }
   @RequestMapping(value = "/uncancelService", method = RequestMethod.GET)
   public ModelAndView cancel(@RequestParam int serviceeId) {
      ModelAndView result;
      try {

         Servicee servicee = serviceeService.findOne(serviceeId);

         if(servicee.isCancelled()){
            servicee.setCancelled(false);
            serviceeService.save(servicee);
         }
         result = new  ModelAndView("redirect:../servicee/listAll.do");
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }

      return result;
   }

   // Ancillary methods ------------------------------------------------

   protected ModelAndView createEditModelAndView(Administrator administrator) {
      ModelAndView result;

      result = createEditModelAndView(administrator, null);

      return result;
   }

   protected ModelAndView createEditModelAndView(Administrator administrator, String message) {
      ModelAndView result;

      result = new ModelAndView("administrator/edit");
      result.addObject("administrator", administrator);
      result.addObject("message", message);

      return result;

   }

   @RequestMapping(value = "/removeComment", method = RequestMethod.GET)
   public ModelAndView removeComment(int comentId) {
      ModelAndView result;
      try {
         Comment commentToDelete = commentService.findOne(comentId);
         administratorService.removeComment(commentToDelete);
         result = new ModelAndView("administrator/success");
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/removeRndz", method = RequestMethod.GET)
   public ModelAndView removeRndz(int rndzId) {
      ModelAndView result;
      try {
         Rendezvous rendezvous = rendezvousService.findOne(rndzId);
         administratorService.removeRendezvouz(rendezvous);
         result = new ModelAndView("administrator/success");
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }

      return result;
   }

   @RequestMapping(value = "/removeAnnouncement", method = RequestMethod.GET)
   public ModelAndView removeAnnouncement(int announcementId) {
      ModelAndView result;
      try {
         Announcement announcement = announcementService.findOne(announcementId);
         administratorService.removeAnnoucement(announcement);
         result = new ModelAndView("administrator/success");
      } catch (Throwable oops) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", oops.getMessage());
      }

      return result;
   }


   @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
   public ModelAndView dashboard() {

      ModelAndView result;

      try {
         Double q1 = administratorService.averageOfRendezvousCreatedPerUser();
         Double q2 = administratorService.standarDesviationOfRendezvousCreatedPerUser();
         Integer q3 = administratorService.numberOfUsersWhoHasCreatedRendezvous();
         Integer q4 = administratorService.numberOfUsersWhoNeverCreatesRendezvous();
         Double q5 = administratorService.averageNumberOfUsersPerRendezvous();
         Double q6 = administratorService.standarDesviationOfUserPerRendezvous();
         Double q7 = administratorService.averageOfRendezvousRSVPPerUser();
         Double q8 = administratorService.standarDesviationOfRendesvousRSVPerUser();

         List<Rendezvous> q10 = administratorService.topTenRendezvousWithMoreRSVP();

         Double q11 = administratorService.averageOfAnnoucmentPerRendezvous();
         Double q12 = administratorService.standarDesviationOfAnnoucementPerRendezvous();

         Collection<Rendezvous> q13 = administratorService.rendezvousWithMoreThan75PerCentOfAnnoucmenetThatTheAverage();
         Collection<Rendezvous> q14 = administratorService.rendezvousWithMoreLinkThanTheAveragePlusTen();


         Double q15 = administratorService.averageNumberOfLinkedInRendezvous();
         Double q16 = administratorService.averageNumberOfQuestionsPerRendezvous();
         Double q17 = administratorService.standarDesviationOfQuestionsPerRendezvous();
         Double q18 = administratorService.averageNumberOfAnswerPerRendezvous();
         Double q19 = administratorService.standarDesviationOfAnswerPerRendezvous();
         Double q20 = administratorService.averageNumberOfRepliesForComment();
         Double q21 = administratorService.standarDesviationOfRepliesForComments();

         Servicee q22=administratorService.bestSellingservice();
         Collection<Manager> q23=administratorService.ManagerWithMoreServicesThanAverage();
         Collection<Manager>  q24=administratorService.managerWithMoreServicesCancelled();
         Collection<Double> q25=administratorService.avgMinMaxDevServicesPerRendezvous();

         Double q26 = administratorService.averageNumberOfServicePerCategory();
         Collection<Servicee> q27 = administratorService.topSellingServices();




         result = new ModelAndView("administrator/dashboard");


         result.addObject("q1", q1);
         result.addObject("q2", q2);
         result.addObject("q3", q3);
         result.addObject("q4", q4);
         result.addObject("q5", q5);
         result.addObject("q6", q6);
         result.addObject("q7", q7);
         result.addObject("q8", q8);
         result.addObject("q10", q10);
         result.addObject("q11", q11);
         result.addObject("q12", q12);
         result.addObject("q13", q13);
         result.addObject("q14", q14);
         result.addObject("q15", q15);
         result.addObject("q16", q16);
         result.addObject("q17", q17);
         result.addObject("q18", q18);
         result.addObject("q19", q19);
         result.addObject("q20", q20);
         result.addObject("q21", q21);
         result.addObject("q22",q22);
         result.addObject("q23",q23);
         result.addObject("q24",q24);
         result.addObject("q25",q25);
         result.addObject("q26",q26);
         result.addObject("q27",q27);


      } catch (Exception e) {
         result = new ModelAndView("administrator/error");
         result.addObject("trace", e.getMessage());
      }


      return result;

   }



   //*************BUSSINESNAME

   //Constructors----------------------------------------------


   protected static ModelAndView createEditModelAndViewB(BussinesName bussinesName) {
      ModelAndView result;

      result= createEditModelAndViewB(bussinesName, null);

      return result;
   }



   protected static ModelAndView createEditModelAndViewB(BussinesName bussinesName, String message) {
      ModelAndView result;

      result = new ModelAndView("administrator/editB");
      result.addObject("bussinesName", bussinesName);
      result.addObject("message", message);

      return result;

   }



   // Ancillary methods ------------------------------------------------


   @RequestMapping(value="/bussinesNameEdit", method=RequestMethod.GET)
   public ModelAndView editB(){
      ModelAndView result;
      BussinesName bussinesName;

      List<BussinesName> bussinesNames = new ArrayList<>(bussinesNameService.findAll());
      bussinesName = bussinesNames.get(0);
      result= createEditModelAndViewB(bussinesName);

      return result;
   }


   @RequestMapping(value="/editB", method=RequestMethod.POST, params="save")
   public ModelAndView saveB(@Valid BussinesName bussinesName, BindingResult binding) {
      ModelAndView result;
      if (binding.hasErrors()) {
         result = createEditModelAndViewB(bussinesName);
      } else {
         try {
            bussinesNameService.save(bussinesName);
            result = new ModelAndView("administrator/success");
         } catch (Throwable oops) {
            result = createEditModelAndViewB(bussinesName, "bussinesName.commit.error");
         }
      }
      return result;

   }




   //**************************WELCOME MEZZAGE******

   protected static ModelAndView createEditModelAndViewW(WelcomePhrase welcomePhrase) {
      ModelAndView result;

      result= createEditModelAndViewW(welcomePhrase, null);

      return result;
   }



   protected static ModelAndView createEditModelAndViewW(WelcomePhrase welcomePhrase, String message) {
      ModelAndView result;

      result = new ModelAndView("administrator/editW");
      result.addObject("welcomePhrase", welcomePhrase);
      result.addObject("message", message);

      return result;

   }



   //Create Method -----------------------------------------------------------




   // Ancillary methods ------------------------------------------------


   @RequestMapping(value="/welcomePhraseEdit", method=RequestMethod.GET)
   public ModelAndView editW(){
      ModelAndView result;
      WelcomePhrase welcomePhrase;

      List<WelcomePhrase> welcomePhrases = new ArrayList<>(welcomePhraseService.findAll());
      welcomePhrase = welcomePhrases.get(0);
      Assert.notNull(welcomePhrase);
      result= createEditModelAndViewW(welcomePhrase);

      return result;
   }


   @RequestMapping(value="/editW", method=RequestMethod.POST, params="save")
   public ModelAndView saveW(@Valid WelcomePhrase welcomePhrase, BindingResult binding){
      ModelAndView result;
      if (binding.hasErrors()) {
         result= createEditModelAndViewW(welcomePhrase);
      }else{
         try{
            welcomePhraseService.save(welcomePhrase);
            result = new ModelAndView("administrator/success");
         }catch(Throwable oops){
            result= createEditModelAndViewW(welcomePhrase, "welcomePhrase.commit.error");
         }
      }
      return result;
   }

}
