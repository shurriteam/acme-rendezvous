package controllers;


import domain.WelcomePhrase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.WelcomePhraseService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/welcomePhrase")
public class WelcomePhraseController extends AbstractController {

	//Services ----------------------------------------------------------------

	@Autowired
	private WelcomePhraseService welcomePhraseService;



	//Constructors----------------------------------------------

	public WelcomePhraseController(){
		super();
	}

    protected static ModelAndView createEditModelAndViewW(WelcomePhrase welcomePhrase) {
        ModelAndView result;

        result= createEditModelAndViewW(welcomePhrase, null);

        return result;
    }



    protected static ModelAndView createEditModelAndViewW(WelcomePhrase welcomePhrase, String message) {
        ModelAndView result;

        result = new ModelAndView("welcomePhrase/edit");
        result.addObject("welcomePhrase", welcomePhrase);
        result.addObject("message", message);

        return result;

    }



    //Create Method -----------------------------------------------------------




    // Ancillary methods ------------------------------------------------


    @RequestMapping(value="/edit", method=RequestMethod.GET)
    public ModelAndView edit(@RequestParam int welcomePhraseId){
        ModelAndView result;
        WelcomePhrase welcomePhrase;

        welcomePhrase= welcomePhraseService.findOne(welcomePhraseId);
        Assert.notNull(welcomePhrase);
        result= createEditModelAndViewW(welcomePhrase);

        return result;
    }


    @RequestMapping(value="/edit", method=RequestMethod.POST, params="save")
    public ModelAndView save(@Valid WelcomePhrase welcomePhrase, BindingResult binding){
        ModelAndView result;
        if (binding.hasErrors()) {
            result= createEditModelAndViewW(welcomePhrase);
        }else{
            try{
                welcomePhraseService.save(welcomePhrase);
                result= new ModelAndView("redirect:list.do");
            }catch(Throwable oops){
                result= createEditModelAndViewW(welcomePhrase, "welcomePhrase.commit.error");
            }
        }
        return result;
    }



}
