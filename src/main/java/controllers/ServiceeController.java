package controllers;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;


@Controller
@RequestMapping("/servicee")
public class ServiceeController {

    @Autowired
    private ServiceeService serviceeService;
    @Autowired
    private ManagerService managerService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private RendezvousService rendezvousService;
    @Autowired
    private UserService userService;


    @RequestMapping(value = "/create", method = RequestMethod.GET)

    public ModelAndView create() {

        ModelAndView result;


        try {
            Servicee servicee = serviceeService.create();
            Manager manager = managerService.findByPrincipal();
            servicee.setOwner(manager);
            result = createEditModelAndView(servicee);
            result.addObject("categoriezz", categoryService.findAll());
            result.addObject("servicee", servicee);


        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
        }
        return result;
    }

        @RequestMapping(value = "/listAll", method = RequestMethod.GET)
        public ModelAndView rendezvousListAll () {

            ModelAndView result;
            Collection<Servicee> servicees;

            try {
                servicees = serviceeService.findAll();
                result = new ModelAndView("servicee/list");
                result.addObject("servicees", servicees);
                result.addObject("requestURI", "servicee/listAll.do");
            } catch (Exception e) {
                result = new ModelAndView("administrator/error");
                result.addObject("trace", e.getMessage());
            }

            return result;
        }

        @RequestMapping(value = "/listCategories", method = RequestMethod.GET)
        public ModelAndView servicePerCategories (@RequestParam int categoryId) {

            ModelAndView result;
            Collection<Servicee> servicees;

            try {
                Category category = categoryService.findOne(categoryId);
                servicees = serviceeService.serviceesByCategory(category);
                result = new ModelAndView("servicee/list");
                result.addObject("servicees", servicees);
                result.addObject("requestURI", "servicee/listAll.do");
            } catch (Exception e) {
                result = new ModelAndView("administrator/error");
                result.addObject("trace", e.getMessage());
            }

            return result;
        }


        @RequestMapping(value = "/listToManager", method = RequestMethod.GET)
        public ModelAndView rendezvousListToAssist () {

            ModelAndView result;
            Collection<Servicee> servicees;
            // Boolean my = true;

            try {
                Manager logedManeger = managerService.findByPrincipal();
                servicees = logedManeger.getServices();
                result = new ModelAndView("servicee/list");
                result.addObject("servicees", servicees);
                // result.addObject("my", my);
                result.addObject("requestURI", "servicee/listToManager.do");
            } catch (Exception e) {
                result = new ModelAndView("administrator/error");
                result.addObject("trace", e.getMessage());
            }

            return result;
        }






    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int serviceeId) {

        ModelAndView result;
        Servicee servicee;

        try {
            Assert.notNull(serviceeId);
            servicee = serviceeService.findOne(serviceeId);
            if(managerService.findByPrincipal().getServices().contains(servicee)){

                result = createEditModelAndView(servicee);
                result.addObject("servicee", servicee);
                result.addObject("categoriezz", categoryService.findAll());

            }else {
                throw new IllegalArgumentException("You are not the owner of this servicee");
            }

        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());

        }


        return result;
    }

   @RequestMapping(value = "/edit", method = RequestMethod.POST,params = "save")
   public ModelAndView edit(@Valid  Servicee servicee, BindingResult bindingResult){
      ModelAndView result;
      if(bindingResult.hasErrors())
         result = createEditModelAndView(servicee);
      else{
         try{
            serviceeService.save(servicee);
            result = new ModelAndView("redirect: listToManager.do");

         }catch (Throwable oops){
            result = createEditModelAndView(servicee,"servicee.save.error");
            result.addObject("categoriezz", categoryService.findAll());
         }
      }
      return result;
   }
    @RequestMapping(value = "/edit", method = RequestMethod.POST,params = "delete")
    public ModelAndView delete(Servicee servicee) {
        ModelAndView result;


        try {
             if(managerService.findByPrincipal().getServices().contains(servicee)){

                if (servicee.getRendezvous().isEmpty()){
                    serviceeService.delete(servicee);
                    result = new ModelAndView("redirect:listToManager.do");
                }else{
                    throw new IllegalArgumentException("The service is associated with a rendezvous");
                }

             }else {

                 throw new IllegalArgumentException("You are not the owner of this servicee.");

             }

        } catch (Throwable oops) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", oops.getMessage());
        }

        return result;
    }












    // Ancillary methods ------------------------------------------------

    protected ModelAndView createEditModelAndView(Servicee servicee) {
        ModelAndView result;

        result = createEditModelAndView(servicee, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(Servicee servicee, String message) {
        ModelAndView result;



        result = new ModelAndView("servicee/edit");
        result.addObject("servicee", servicee);
        result.addObject("message", message);

        return result;

    }

}