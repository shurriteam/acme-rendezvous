/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Announcement;
import domain.Rendezvous;
import domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {


   @Query("select c from User c where c.userAccount.id = ?1")
   User findByUserAccountId(int userAccountId);

   @Query("select c from User c join c.userAccount u join u.authorities a where a.authority = 'USER'")
   Collection<User> explorers();

   @Query("select myrend from User u join u.createdRendez myrend where u.id =?1")
   Collection<Rendezvous> createdRenezByUser(int userId);


   @Query("select myrend.announcements from User u join u.createdRendez myrend where u.id =?1")
   Collection<Announcement> createdAnnouncementsByUser(int userId);
   	
   @Query("select an from User c join c.rendezToAssist rd join rd.announcements an where c.id = ?1 order by an.creationMomment")
   Collection<Announcement> allMyAnnouncement(int userId);
}
