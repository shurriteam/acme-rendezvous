package repositories;

import domain.Banner;
import domain.BussinesName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by daviddelatorre on 10/3/17.
 */
@Repository
public interface BussinesNameRepository extends JpaRepository<BussinesName, Integer> {

}
