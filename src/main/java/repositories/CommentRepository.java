/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Comment;
import domain.Rendezvous;
import domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Created by david on 05/11/2016.
 */

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {


   @Query("select re from Rendezvous re join re.comments c where c.id = ?1")
   Collection<Rendezvous> rendezvousesWithGivenComment(int commentId);

   @Query("select re from User re join re.createdComments c where c.id = ?1")
   Collection<User> usersWithGivenComment(int commentId);
}
