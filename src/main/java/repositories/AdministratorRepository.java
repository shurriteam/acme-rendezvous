/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 28/3/17.
 */
@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

   @Query("select c from Administrator c where c.userAccount.id = ?1")
   Administrator findByUserAccountId(int userAccountId);


   @Query("select c from Administrator c join c.userAccount u join u.authorities a where a.authority = 'ADMINISTRATOR'")
   Collection<Administrator> administrators();


   @Query("select avg(t.createdRendez.size) from User t")
   Double averageOfRendezvousCreatedPerUser();

   @Query("select stddev(t.createdRendez.size) from User t")
   Double standarDesviationOfRendezvousCreatedPerUser();

   @Query("select c from User c where c.createdRendez.size != 0")
   Collection<User> numberOfUsersWhoHasCreatedRendezvous(); //Esta

   @Query("select c from User c where c.createdRendez.size = 0")
   Collection<User> numberOfUsersWhoNeverCreatesRendezvous(); //Esta

   @Query("select avg(t.assistants.size) from Rendezvous t")
   Double averageNumberOfUsersPerRendezvous();
   @Query("select stddev(t.assistants.size) from Rendezvous t")
   Double standarDesviationOfUserPerRendezvous();

   @Query("select avg(t.rendezToAssist.size) from User t")
   Double averageOfRendezvousRSVPPerUser();
   @Query("select stddev(t.rendezToAssist.size) from User t")
   Double standarDesviationOfRendesvousRSVPerUser();

   @Query("select c from Rendezvous c order by c.assistants.size")
   Collection<Rendezvous> topTenRendezvousWithMoreRSVP();

   @Query("select avg(t.announcements.size) from Rendezvous t")
   Double averageOfAnnoucmentPerRendezvous();
   @Query("select stddev(t.announcements.size) from Rendezvous t")
   Double standarDesviationOfAnnoucementPerRendezvous();

   @Query("select c from Rendezvous c where c.announcements.size > (select avg(t.announcements.size) from Rendezvous t) * 0.75")
   Collection<Rendezvous> rendezvousWithMoreThan75PerCentOfAnnoucmenetThatTheAverage();

   @Query("select avg(t.related.size) from Rendezvous t")
   Double averageNumberOfLinkedInRendezvous();
   @Query("select c from Rendezvous c where c.related.size > (select avg(t.related.size) from Rendezvous t) + 0.1")
   Collection<Rendezvous> rendezvousWithMoreLinkThanTheAveragePlusTen();

   @Query("select avg(t.questions.size) from Rendezvous t")
   Double averageNumberOfQuestionsPerRendezvous();
   @Query("select stddev(t.questions.size) from Rendezvous t")
   Double standarDesviationOfQuestionsPerRendezvous();

   @Query("select avg(f.answers.size) from Rendezvous t join t.forms f")
   Double averageNumberOfAnswerPerRendezvous();
   @Query("select stddev(f.answers.size) from Rendezvous t join t.forms f")
   Double standarDesviationOfAnswerPerRendezvous();

   @Query("select avg(t.reply.size) from Comment t ")
   Double averageNumberOfRepliesForComment();
   @Query("select stddev(t.reply.size) from Comment t")
   Double standarDesviationOfRepliesForComments();



   //****************? The best-selling services Nivelc************

  // @Query("select s from Servicee s order by s.description desc "  )


   @Query("select s from Servicee s order by s.rendezvous.size desc ")
   Collection<Servicee> bestSellingservice();

   @Query(" select m from Manager m where m.services.size >(select avg(m.services.size) from Manager m)")
   Collection<Manager> ManagerWithMoreServicesThanAverage();

   @Query("select s.owner from Servicee s where s.cancelled=true group by s.owner order by count(s) desc")
   Collection<Manager> managerWithMoreservicesCancelled();




   @Query("select avg(r.servicees.size), min(r.servicees.size),max(r.servicees.size), sqrt(sum(r.servicees.size * r.servicees.size)/count(s) - (avg(r.servicees.size) * avg(r.servicees.size ))) from Rendezvous r, Servicee  s")
   Collection<Double> avgMinMaxDevServicesPerRendezvous();

   @Query("select avg(t.associatedServices.size) from Category t")
   Double averageNumberOfServicePerCategory();

   @Query("select s from Servicee s order by s.rendezvous.size desc")
   Collection<Servicee> topSellingServices();

}
