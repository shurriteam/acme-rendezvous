/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.Category;
import domain.Servicee;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Created by daviddelatorre on 28/3/17.
 */
@Repository
public interface ServiceeRepository extends JpaRepository<Servicee, Integer> {


    @Query("select c.associatedServices from Category c where c = ?1")
    Collection<Servicee> serviceesByCategory(Category category);

}
