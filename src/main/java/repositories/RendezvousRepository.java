/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package repositories;

import domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface RendezvousRepository extends JpaRepository<Rendezvous, Integer> {

	@Query("select fm from Rendezvous c join c.forms fm where fm.owner.id = ?1 and c.id = ?2")
	Form formwithGivenUser(int userId, int rendezvousId);

   @Query("select an from Rendezvous re join re.announcements an where re.id = ?1")
   Collection<Announcement> announcementsGivenARendez(int rendezvousId);


   @Query("select ra from Rendezvous re join re.related ra where re.id = ?1")
   Collection<Rendezvous> relatedRendezsGivenARendez(int rendezvousId);

   @Query("select ra from Rendezvous re join re.servicees ra where re.id = ?1")
   Collection<Servicee> relatedRendezsGivenAServicee(int rendezvousId);


   @Query("select que from Rendezvous re join re.questions que where re.id = ?1")
   Collection<Question> relatedQuestionsGivenARendez(int rendezvousId);

   @Query("select form from Rendezvous re join re.forms form where re.id = ?1")
   Collection<Form> relatedFormsGivenARendez(int rendezvousId);
   @Query("select re from Rendezvous re where re.adult=TRUE ")
   Collection<Rendezvous> rendezForAdults();


   @Query("select re from Rendezvous re where re.adult =false")
   Collection<Rendezvous> rendezForAllAges();




}
