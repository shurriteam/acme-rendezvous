/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;


@Entity
@Access(AccessType.PROPERTY)
public class Servicee extends DomainEntity {


	private String name, description, url;
	private Manager owner;
    private boolean cancelled;
	private Category categorie;
	private Collection<Rendezvous> rendezvous;




   	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	@URL
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@ManyToOne
	public Manager getOwner() {
		return owner;
	}
	public void setOwner(Manager owner) {
		this.owner = owner;
	}

	@NotNull
	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}


   @ManyToOne(cascade = CascadeType.PERSIST)
   public Category getCategorie() {
      return categorie;
   }

   public void setCategorie(Category categorie) {
      this.categorie = categorie;
   }

   @ManyToMany
	public Collection<Rendezvous> getRendezvous() {
		return rendezvous;
	}

	public void setRendezvous(Collection<Rendezvous> rendezvous) {
		this.rendezvous = rendezvous;
	}
}
