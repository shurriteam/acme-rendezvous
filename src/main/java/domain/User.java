/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import javax.persistence.*;
import java.util.Collection;


@Entity
@Access(AccessType.PROPERTY)
public class User extends Actor {

   private Collection<Rendezvous> createdRendez;
   private Collection<Rendezvous> rendezToAssist;
   private Collection<Comment> createdComments;
   private CreditCard creditCard;


   @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "owner")
   public Collection<Rendezvous> getCreatedRendez() {
      return createdRendez;
   }

   public void setCreatedRendez(Collection<Rendezvous> createdRendez) {
      this.createdRendez = createdRendez;
   }

   @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "assistants")
   public Collection<Rendezvous> getRendezToAssist() {
      return rendezToAssist;
   }

   public void setRendezToAssist(Collection<Rendezvous> rendezToAssist) {
      this.rendezToAssist = rendezToAssist;
   }

   @OneToMany(cascade = CascadeType.PERSIST)
   public Collection<Comment> getCreatedComments() {
      return createdComments;
   }

   public void setCreatedComments(Collection<Comment> createdComments) {
      this.createdComments = createdComments;
   }

   @OneToOne(cascade = CascadeType.ALL)
   public CreditCard getCreditCard() {
      return creditCard;
   }

   public void setCreditCard(CreditCard creditCard) {
      this.creditCard = creditCard;
   }
}
