package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;


@Entity
@Access(AccessType.PROPERTY)
public class Rendezvous extends DomainEntity {
   private String name, description, picture;
   private Boolean adult;
   private Date organizedMomment, publishMomment;
   private Coordinate localization;
   private User owner;
   private RendezStatus rendezStatus;
   private Collection<User> assistants;
   private Collection<Rendezvous> related;
   private Collection<Announcement> announcements;
   private Collection<Comment> comments;
   private Collection<Question> questions;
   private Collection<Form> forms;
   private Collection<Servicee> servicees;


   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   @NotBlank
   @URL
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getPicture() {
      return picture;
   }

   public void setPicture(String picture) {
      this.picture = picture;
   }

   @Temporal(TemporalType.DATE)
   public Date getOrganizedMomment() {
      return organizedMomment;
   }

   public void setOrganizedMomment(Date organizedMomment) {
      this.organizedMomment = organizedMomment;
   }

   @Temporal(TemporalType.DATE)
   public Date getPublishMomment() {
      return publishMomment;
   }

   public void setPublishMomment(Date publishMomment) {
      this.publishMomment = publishMomment;
   }

   @Valid
   @OneToOne
   public Coordinate getLocalization() {
      return localization;
   }

   public void setLocalization(Coordinate localization) {
      this.localization = localization;
   }

   @ManyToOne(cascade = CascadeType.REFRESH)
   public User getOwner() {
      return owner;
   }


   public void setOwner(User owner) {
      this.owner = owner;
   }

   @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.PERSIST})
   public Collection<User> getAssistants() {
      return assistants;
   }

   public void setAssistants(Collection<User> assistants) {
      this.assistants = assistants;
   }

   @ManyToMany(cascade = CascadeType.PERSIST)
   public Collection<Rendezvous> getRelated() {
      return related;
   }

   public void setRelated(Collection<Rendezvous> related) {
      this.related = related;
   }

   @OneToMany(cascade = CascadeType.ALL, mappedBy = "rendezvous")
   public Collection<Announcement> getAnnouncements() {
      return announcements;
   }

   public void setAnnouncements(Collection<Announcement> announcements) {
      this.announcements = announcements;
   }

   @OneToMany(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
   public Collection<Comment> getComments() {
      return comments;
   }

   public void setComments(Collection<Comment> comments) {
      this.comments = comments;
   }

   @NotNull
   public Boolean getAdult() {
      return adult;
   }

   public void setAdult(Boolean adult) {
      this.adult = adult;
   }

   @OneToMany(cascade = CascadeType.REMOVE)
   public Collection<Question> getQuestions() {
      return questions;
   }

   public void setQuestions(Collection<Question> questions) {
      this.questions = questions;
   }

   @OneToMany(mappedBy = "rendezvous", cascade = CascadeType.REMOVE)
   public Collection<Form> getForms() {
      return forms;
   }

   public void setForms(Collection<Form> forms) {
      this.forms = forms;
   }

   public RendezStatus getRendezStatus() {
      return rendezStatus;
   }

   public void setRendezStatus(RendezStatus rendezStatus) {
      this.rendezStatus = rendezStatus;
   }

   @ManyToMany
   public Collection<Servicee> getServicees() {
      return servicees;
   }

   public void setServicees(Collection<Servicee> servicees) {
      this.servicees = servicees;
   }

   @Override
   public String toString() {
      return name + " {" + description +
              '}';
   }
}

