package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class WelcomePhrase extends DomainEntity {


    private String spanishPhrase, englishPhrase;

    // Relationships ---------------------------------------------------------



    //Constructor
    public WelcomePhrase(){
        super();
    }


    @NotBlank
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getSpanishPhrase() {
        return spanishPhrase;
    }

    public void setSpanishPhrase(String spanishPhrase) {
        this.spanishPhrase = spanishPhrase;
    }
    @NotBlank
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getEnglishPhrase() {
        return englishPhrase;
    }

    public void setEnglishPhrase(String englishPhrase) {
        this.englishPhrase = englishPhrase;
    }
}
