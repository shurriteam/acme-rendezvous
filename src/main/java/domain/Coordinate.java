package domain;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Coordinate extends DomainEntity {
   private Double longuitide, latitude;
   private String longDirection, latDirection;

   @NotNull
   public Double getLonguitide() {
      return longuitide;
   }

   public void setLonguitide(Double longuitide) {
      this.longuitide = longuitide;
   }

   @NotNull
   public Double getLatitude() {
      return latitude;
   }

   public void setLatitude(Double latitude) {
      this.latitude = latitude;
   }

   @NotBlank
   @Length(min = 1, max = 2)
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getLongDirection() {
      return longDirection;
   }

   public void setLongDirection(String longDirection) {
      this.longDirection = longDirection;
   }

   @NotBlank
   @Length(min = 1, max = 2)
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getLatDirection() {
      return latDirection;
   }

   public void setLatDirection(String latDirection) {
      this.latDirection = latDirection;
   }

   @Override
   public String toString() {
      return "Coordinates: " + longuitide + "[" + longDirection + "] " + latitude + "[" + latDirection + "]";
   }
}
