package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Announcement extends DomainEntity {

   private Date creationMomment;
   private String title, description;
   private Rendezvous rendezvous;


   @Temporal(TemporalType.DATE)
   public Date getCreationMomment() {
      return creationMomment;
   }

   public void setCreationMomment(Date creationMomment) {
      this.creationMomment = creationMomment;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   @ManyToOne(cascade = CascadeType.PERSIST)
   public Rendezvous getRendezvous() {
      return rendezvous;
   }

   public void setRendezvous(Rendezvous rendezvous) {
      this.rendezvous = rendezvous;
   }
}
