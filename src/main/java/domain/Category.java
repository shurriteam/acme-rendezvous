/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import java.util.Collection;


@Entity
@Access(AccessType.PROPERTY)
public class Category extends DomainEntity {


	private String name;
	private String description;
   private Category father;
	private Collection<Category> sons;
	private Collection<Servicee> associatedServices;

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany
	public Collection<Category> getSons() {
		return sons;
	}

	public void setSons(Collection<Category> sons) {
		this.sons = sons;
	}

	@OneToMany(mappedBy = "categorie")
	public Collection<Servicee> getAssociatedServices() {
		return associatedServices;
	}

	public void setAssociatedServices(Collection<Servicee> associatedServices) {
		this.associatedServices = associatedServices;
	}
   @OneToOne(cascade = CascadeType.PERSIST)
   public Category getFather() {
      return father;
   }

   public void setFather(Category father) {
      this.father = father;
   }

   @Override
   public String toString() {
      return  name;
   }
}
