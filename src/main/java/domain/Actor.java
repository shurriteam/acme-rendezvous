/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.*;
import security.UserAccount;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@Entity
@Access(AccessType.PROPERTY)
public abstract class Actor extends DomainEntity {


   private String name;
   private String surname;
   private String phoneNumber;
   private String email;
   private String address;
   private UserAccount userAccount;
   private int age;



   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @NotNull
   public int getAge() {
      return age;
   }

   public void setAge(int age) {
      this.age = age;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getSurname() {
      return surname;
   }

   public void setSurname(String surname) {
      this.surname = surname;
   }

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }


   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   //@Pattern(regexp = "(\\+\\d{1,3})?(\\(\\d{3}\\))?([0-9a-zA-z][ -]?){4,}")
   public String getPhoneNumber() {
      return phoneNumber;
   }

   public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
   }

   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   @Email
   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }


   @NotNull
   @Valid
   @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, optional = false)
   public UserAccount getUserAccount() {
      return userAccount;
   }

   public void setUserAccount(UserAccount userAccount) {
      this.userAccount = userAccount;
   }


   @Override
   public String toString() {
      return name + ", " + surname + " [" + phoneNumber + "]";
   }
}
