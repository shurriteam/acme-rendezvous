package domain;

public enum RendezStatus {

   DRAFT, FINAL
}
