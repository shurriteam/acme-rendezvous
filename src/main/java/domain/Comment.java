/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;


@Entity
@Access(AccessType.PROPERTY)
public class Comment extends DomainEntity {


   private String text;
   private String description;
   private User owner;
   private int rndId;
   private Collection<Comment> reply;


   @NotBlank
   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getText() {
      return text;
   }

   public void setText(String title) {
      this.text = title;
   }

   @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
   public String getDescription() {
      return description;
   }

   public void setDescription(String text) {
      this.description = text;
   }

   @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
   public User getOwner() {
      return owner;
   }

   public void setOwner(User owner) {
      this.owner = owner;
   }

   @OneToMany(cascade = CascadeType.REMOVE)
   public Collection<Comment> getReply() {
      return reply;
   }

   public void setReply(Collection<Comment> reply) {
      this.reply = reply;
   }


   public int getRndId() {
      return rndId;
   }

   public void setRndId(int rndId) {
      this.rndId = rndId;
   }

   @Override
   public String toString() {
      return "Comment{" +
              "title='" + text + '\'' +
              ", text='" + description + '\'' +
              '}';
   }
}
