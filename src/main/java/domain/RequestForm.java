package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import javax.persistence.*;

@Entity
@Access(AccessType.PROPERTY)
public class RequestForm extends DomainEntity {


    private Rendezvous rendezvous;
    private String comment;
    private Servicee servicee;
    //private CreditCard creditCard;
    @OneToOne

    public Rendezvous getRendezvous() {
        return rendezvous;
    }

    public void setRendezvous(Rendezvous rendezvous) {
        this.rendezvous = rendezvous;
    }

    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    @OneToOne
    public Servicee getServicee() {
        return servicee;
    }

    public void setServicee(Servicee servicee) {
        this.servicee = servicee;
    }






}
