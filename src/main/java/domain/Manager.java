/*
 * Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
 */

package domain;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

import java.util.Collection;


@Entity
@Access(AccessType.PROPERTY)
public class Manager extends Actor {

  
	private String VAT;
	private Collection<Servicee> services;

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getVAT() {
		return VAT;
	}

	public void setVAT(String vAT) {
		VAT = vAT;
	}

	@OneToMany(mappedBy = "owner")
	public Collection<Servicee> getServices() {
		return services;
	}

	public void setServices(Collection<Servicee> services) {
		this.services = services;
	}
	
	
	


}
