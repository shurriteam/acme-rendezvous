package converters;

import domain.Announcement;
import domain.Answer;
import repositories.AnswerRepository;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import security.UserAccount;
import security.UserAccountRepository;
import services.AnnouncementService;

@Component
@Transactional
public class StringToAnswerConverter implements Converter<String, Answer> {

   @Autowired
   AnswerRepository userAccountRepository;

   @Override
   public Answer convert(String text) {
	   Answer result;
      int id;

      try {
         if (StringUtils.isEmpty(text))
            result = null;
         else {
            id = Integer.valueOf(text);
            result = userAccountRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}