
package converters;

import domain.Actor;
import domain.Announcement;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class AnnouncementToStringConverter implements Converter<Announcement, String> {

   @Override
   public String convert(Announcement actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }

}
