
package converters;

import domain.Actor;
import domain.Announcement;
import domain.Answer;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class AnswerToStringConverter implements Converter<Answer, String> {

   @Override
   public String convert(Answer actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }

}
