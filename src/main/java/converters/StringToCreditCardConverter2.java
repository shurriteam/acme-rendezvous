package converters;

import domain.Administrator;
import domain.CreditCard;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.AdministratorRepository;
import repositories.CreditCardRepository;


@Component
@Transactional
public class StringToCreditCardConverter2 implements Converter<String, CreditCard> {
   @Autowired
   CreditCardRepository administratorRepository;

   @Override
   public CreditCard convert(String text) {
	   CreditCard result;
      int id;

      try {
         if
                 (StringUtils.isEmpty(text))
            result = null;
         else {
            id = Integer.valueOf(text);
            result = administratorRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}
