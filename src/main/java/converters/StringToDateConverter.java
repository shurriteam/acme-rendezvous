package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

import java.util.Date;

public class StringToDateConverter implements Converter<String, Date> {

   @SuppressWarnings("deprecation")
   @Override
   public Date convert(String source) {
      Date result;
      try {
         if (StringUtils.isEmpty(source))
            result = null;
         else {
            if (source.length() > 10) {
               String aux = source.substring(0, source.length() - 2);
               return new Date(aux);

            }
            result = new Date(source);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }
      return result;
   }
}