
package converters;

import domain.BussinesName;
import domain.WelcomePhrase;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class WelcomePhraseToStringConverter implements Converter<WelcomePhrase, String> {

   @Override
   public String convert(WelcomePhrase actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }

}
