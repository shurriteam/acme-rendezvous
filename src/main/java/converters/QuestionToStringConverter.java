
package converters;

import domain.Actor;
import domain.Announcement;
import domain.Question;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class QuestionToStringConverter implements Converter<Question, String> {

   @Override
   public String convert(Question actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }

}
