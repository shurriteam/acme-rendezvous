package converters;

import domain.RequestForm;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class RequestFormToStringConverter implements Converter<RequestForm, String> {

    @Override
    public String convert(RequestForm actor) {
        Assert.notNull(actor);
        String result;
        result = String.valueOf(actor.getId());
        return result;
    }

}

