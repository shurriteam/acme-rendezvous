package converters;


import domain.Category;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import services.CategoryService;

@Component
@Transactional
public class StringToCategoryConverter implements Converter<String, Category> {

   @Autowired
   CategoryService categoryService;

   @Override
   public Category convert(String text) {
	   Category result;
      int id;

      try {
         if (StringUtils.isEmpty(text))
            result = null;
         else {
            id = Integer.valueOf(text);
            result = categoryService.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}