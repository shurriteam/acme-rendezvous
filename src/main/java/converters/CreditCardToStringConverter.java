package converters;

import domain.Coordinate;
import domain.CreditCard;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@Component
@Transactional
public class CreditCardToStringConverter implements Converter<CreditCard, String> {
   @Override
   public String convert(CreditCard actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }
}
