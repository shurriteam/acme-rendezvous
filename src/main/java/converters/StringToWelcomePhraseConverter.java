
package converters;

import domain.BussinesName;
import domain.WelcomePhrase;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.BussinesNameRepository;
import repositories.WelcomePhraseRepository;


@Component
@Transactional
public class StringToWelcomePhraseConverter implements Converter<String, WelcomePhrase> {

   @Autowired
   WelcomePhraseRepository actorRepository;

   @Override
   public WelcomePhrase convert(String text) {
      WelcomePhrase result;
      int id;

      try {
         if (StringUtils.isEmpty(text))
            result = null;
         else {
            id = Integer.valueOf(text);
            result = actorRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}
