package converters;

import domain.Announcement;
import domain.Category;
import domain.Manager;
import domain.Servicee;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import security.UserAccount;
import security.UserAccountRepository;
import services.AnnouncementService;
import services.CategoryService;
import services.ManagerService;
import services.ServiceeService;

@Component
@Transactional
public class StringToServiceeConverter implements Converter<String, Servicee> {

   @Autowired
   ServiceeService userAccountRepository;

   @Override
   public Servicee convert(String text) {
	   Servicee result;
      int id;

      try {
         if (StringUtils.isEmpty(text))
            result = null;
         else {
            id = Integer.valueOf(text);
            result = userAccountRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}