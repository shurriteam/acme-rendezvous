package converters;

import domain.RequestForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.RequestFormRepository;

@Component
@Transactional
public class StringToRequestFormConverter implements Converter<String, RequestForm> {

    @Autowired
    RequestFormRepository requestFormRepository;

    @Override
    public RequestForm convert(String text) {
        RequestForm result;
        int id;

        try {
            if (StringUtils.isEmpty(text))
                result = null;
            else {
                id = Integer.valueOf(text);
                result = requestFormRepository.findOne(id);
            }
        } catch (Throwable oops) {
            throw new IllegalArgumentException(oops);
        }

        return result;

    }

}