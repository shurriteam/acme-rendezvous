package converters;

import domain.Announcement;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import security.UserAccount;
import security.UserAccountRepository;
import services.AnnouncementService;

@Component
@Transactional
public class StringToAnnouncementConverter implements Converter<String, Announcement> {

   @Autowired
   AnnouncementService userAccountRepository;

   @Override
   public Announcement convert(String text) {
      Announcement result;
      int id;

      try {
         if (StringUtils.isEmpty(text))
            result = null;
         else {
            id = Integer.valueOf(text);
            result = userAccountRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}