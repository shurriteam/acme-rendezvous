
package converters;

import domain.Actor;
import domain.Category;
import domain.Servicee;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class ServiceeToStringConverter implements Converter<Servicee, String> {

   @Override
   public String convert(Servicee actor) {
      Assert.notNull(actor);
      String result;
      result = String.valueOf(actor.getId());
      return result;
   }

}
