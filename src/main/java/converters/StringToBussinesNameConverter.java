
package converters;

import domain.Actor;
import domain.BussinesName;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.ActorRepository;
import repositories.BussinesNameRepository;


@Component
@Transactional
public class StringToBussinesNameConverter implements Converter<String, BussinesName> {

   @Autowired
   BussinesNameRepository actorRepository;

   @Override
   public BussinesName convert(String text) {
      BussinesName result;
      int id;

      try {
         if (StringUtils.isEmpty(text))
            result = null;
         else {
            id = Integer.valueOf(text);
            result = actorRepository.findOne(id);
         }
      } catch (Throwable oops) {
         throw new IllegalArgumentException(oops);
      }

      return result;

   }

}
