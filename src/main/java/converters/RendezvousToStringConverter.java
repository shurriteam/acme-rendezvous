package converters;

import domain.Rendezvous;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
@Transactional

public class RendezvousToStringConverter implements Converter<Rendezvous, String> {


   @Override
   public String convert(Rendezvous cat) {
      String res;

      if (cat == null) {
         res = null;
      } else {
         res = String.valueOf(cat.getId());
      }
      return res;
   }
}