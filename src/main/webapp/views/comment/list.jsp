<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>


<!-- Listing grid -->
<display:table pagesize="20" class="displaytag" keepStatus="true"
               name="comment" requestURI="${requestURI}" id="row">


    <!-- Attributes -->

    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a href="comment/delete.do?commentId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="comment.text" var="text"/>
    <display:column property="text" title="${text}" sortable="true"/>
    <spring:message code="comment.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>
    <spring:message code="comment.owner" var="owner"/>
    <display:column property="owner" title="${owner}" sortable="true"/>

</display:table>