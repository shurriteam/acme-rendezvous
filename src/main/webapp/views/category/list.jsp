<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<security:authorize access="hasRole('ADMINISTRATOR')">
    <div>
        <H5>
            <a href="category/create.do"> <spring:message
                    code="general.create"/>
            </a>
        </H5>
    </div>
</security:authorize>

<!-- Listing grid -->
<display:table pagesize="20" class="displaytag" keepStatus="true"
               name="categories" requestURI="${requestURI}" id="row">


    <!-- Attributes -->




    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a href="category/edit.do?categoryId=${row.id}"> <spring:message
                    code="general.edit"/>
            </a>
        </display:column>

        <display:column>
            <a href="category/delete.do?categoryId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="category.name" var="title"/>
    <display:column property="name" title="${title}" sortable="true"/>
    <spring:message code="category.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>
    <spring:message code="category.father" var="father"/>
    <display:column property="father" title="${father}" sortable="true"/>

    <display:column>
        <a href="servicee/listCategories.do?categoryId=${row.id}"> <spring:message
                code="general.view.servicee"/>
        </a>
    </display:column>


</display:table>