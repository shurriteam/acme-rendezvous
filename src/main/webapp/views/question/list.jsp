<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--<jsp:setProperty name="idr" property="${idRe}"/>--%>
	<security:authorize access="hasRole('USER')">
		    <a class="button" href="question/create.do?rendezvousId=${idr}"> <spring:message
		    code="general.create"/>
		    </a>
    </security:authorize> 

<!-- Listing grid -->
<display:table pagesize="15" class="displaytag" keepStatus="true"
               name="questions" requestURI="${requestURI}" id="row">



    <spring:message code="question.text" var="text"/>
    <display:column property="text" title="${text}" sortable="true"/>
    
    <display:column>
		    <a class="button" href="question/edit.do?questionId=${row.id}"> <spring:message
		    code="general.edit"/>
		    </a>
	</display:column>
   
</display:table>