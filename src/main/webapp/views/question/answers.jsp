<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<form:form action="question/answer.do" modelAttribute="form">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="questions"/>
    <form:hidden path="answers"/>
    <form:hidden path="rendezvous"/>


    <c:forEach begin="0" end="${listSize-1}" varStatus="loop">


        <h3><jstl:out value="${form.questions[loop.index].text}"/></h3>
        <acme:textbox path="answers[${loop.index}].text" code="general.save"/>


    </c:forEach>



    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>


    <a class="button" href="/welcome/index.do"><spring:message code="general.cancel"/></a>


</form:form>