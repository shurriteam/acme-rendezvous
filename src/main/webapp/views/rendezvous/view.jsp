<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 1/3/17
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<security:authorize access="hasRole('USER')">
    <jstl:if test="${not auth}">
        <a class="button" href="question/answer.do?rendezvousId=${idR}"> <spring:message
                code="general.craate.rndz"/>
        </a>
    </jstl:if>
    <jstl:if test="${auth}">
        <a class="button" href="question/cancelRNDZ.do?rendezvousId=${idR}"> <spring:message
                code="general.remove.rndz"/>
        </a>
    </jstl:if>
</security:authorize>

<spring:message code="rendezvous.picture" var="picture1"/>
<h3><jstl:out value="${picture1}"/></h3>
<img src="<jstl:out value="${picture}"/>" width="500px" height="100%" alt="image_head">

<spring:message code="rendezvous.name" var="name1"/>
<h3><jstl:out value="${name1}"/></h3>
<jstl:out value="${name}"/>


<spring:message code="rendezvous.description" var="description1"/>
<h3><jstl:out value="${description1}"/></h3>
<jstl:out value="${description}"/>


<spring:message code="rendezvous.momment" var="momment1"/>
<h3><jstl:out value="${momment1}"/></h3>
<jstl:out value="${organizedMomment}"/>

<spring:message code="rendezvous.localization" var="localization1"/>
<h3><jstl:out value="${localization1}"/></h3>
<jstl:out value="${localization}"/>

<spring:message code="rendezvous.owner" var="owner1"/>
<h3><jstl:out value="${owner1}"/></h3>
<jstl:out value="${owner}"/>

<div id="comment-box">

    <spring:message code="rendezvous.comments" var="comments2"/>
    <h2><jstl:out value="${comments2}"/></h2>






    <c:forEach items="${comments}" var="comment">

    <jstl:out value="${comment.text}"/>
        <p style="color: darkgrey"><jstl:out value="${comment.owner}"/></p>
        </br>

    <jstl:out value="${comment.description}"/>
        </br>
        <%--TODO no se como mostrar esta tabla--%>
        <display:table pagesize="15" class="displaytag" keepStatus="true"
        name="comment.reply" requestURI="${requestURI}" id="row">

            <spring:message code="comment.text" var="text"/>
            <display:column property="text" title="${text}" sortable="true"/>
            <spring:message code="comment.description" var="description"/>
            <display:column property="description" title="${description}" sortable="true"/>
            <spring:message code="comment.owner" var="owner"/>
            <display:column property="owner" title="${owner}" sortable="true"/>


        </display:table>

        </br>

    </div>
    </c:forEach>

    <security:authorize access="hasRole('USER')">
        <jstl:if test="${auth}">
            <a class="button2" href="comment/create.do?rendezovousId=${idR}"><spring:message code="general.create"/></a>
        </jstl:if>


    </security:authorize>

    <hr>


        </div>
<div id="announcement-box">

<spring:message code="rendezvous.announcement" var="announcement1"/>
<h2><jstl:out value="${announcement1}"/></h2>

    <display:table pagesize="15" class="displaytag" keepStatus="true"
                   name="announcements" requestURI="${requestURI}" id="row">


        <spring:message code="rendezvous.name" var="title"/>
        <display:column property="title" title="${title}" sortable="true"/>
        <spring:message code="rendezvous.description" var="description"/>
        <display:column property="description" title="${description}" sortable="true"/>


        <spring:message code="rendezvous.momment" var="creationMomment"/>
        <display:column property="creationMomment" title="${creationMomment}" sortable="true"/>

        <security:authorize access="hasRole('USER')">
            <jstl:if test="${isMy}">
                <display:column>
                <a href="announcement/create.do?rendezvousId=${row.id}"> <spring:message
                        code="user.create.announcement"/>
                </a>
                </display:column>
            </jstl:if>
        </security:authorize>


    </display:table>

<hr>

</div>

<div id="realted-box">

<spring:message code="rendezvous.related" var="related1"/>
<h2><jstl:out value="${related1}"/></h2>

    <display:table pagesize="15" class="displaytag" keepStatus="true"
                   name="related" requestURI="${requestURI}" id="row">

        <spring:message code="rendezvous.picture" var="picture"/>
        <display:column title="${picture}">
            <img src="${row.picture}" width="130" height="100">
        </display:column>


        <spring:message code="rendezvous.momment" var="organizedMomment"/>
        <display:column property="organizedMomment" title="${organizedMomment}" sortable="true"/>
        <spring:message code="rendezvous.owner" var="owner"/>
        <display:column property="owner" title="${owner}" sortable="true"/>

        <security:authorize access="isAuthenticated()">
            <display:column>
                <a href="user/view.do?userId=${row.owner.id}"> <spring:message
                        code="user.view"/>
                </a>
            </display:column>
        </security:authorize>

        <security:authorize access="isAuthenticated()">
            <display:column>
                <a href="rendezvous/view.do?rendezvousId=${row.id}"> <spring:message
                        code="rendezvous.view"/>
                </a>
            </display:column>
        </security:authorize>

        <security:authorize access="isAnonymous()">
            <display:column>
                <a href="user/view.do?userId=${row.owner.id}"> <spring:message
                        code="user.view"/>
                </a>
            </display:column>
        </security:authorize>

        <security:authorize access="isAnonymous()">
            <display:column>
                <a href="rendezvous/viewNotAuthenticated.do?rendezvousId=${row.id}"> <spring:message
                        code="rendezvous.view"/>
                </a>
            </display:column>
        </security:authorize>

        <security:authorize access="hasRole('USER')">
            <jstl:if test="${isMy}">
                <display:column>
                    <a href="rendezvous/associate.do?rendezvousId=${row.id}"> <spring:message
                        code="user.associate.rendezvous"/>
                </a>
                </display:column>
            </jstl:if>
    </security:authorize>

    </display:table>


    <%--<c:forEach items="${related}" var="relate">--%>

    <%--<h2><jstl:out value="${relate.name}"/></h2>--%>
    <%--<h2><jstl:out value="${relate.description}"/></h2>--%>
    <%--<h2><jstl:out value="${relate.picture}"/></h2>--%>


    <%--</div>--%>
    <%--</c:forEach>--%>



<hr>
</div>

<div id="assis-box">

    <spring:message code="rendezvous.servicees" var="servicees1"/>
    <h2><jstl:out value="${servicees1}"/></h2>

    <display:table pagesize="15" class="displaytag" keepStatus="true"
                   name="servicees" requestURI="${requestURI}" id="row">

        <spring:message code="servicee.name" var="name"/>
        <display:column property="name" title="${name}" sortable="true"/>

        <spring:message code="servicee.description" var="description"/>
        <display:column property="description" title="${description}" sortable="true"/>

        <spring:message code="servicee.url" var="url"/>
        <display:column property="url" title="${url}" sortable="true"/>

    </display:table>

</div>

<div id="assis-box">

<spring:message code="rendezvous.assistances" var="assistances1"/>
<h2><jstl:out value="${assistances1}"/></h2>

    <display:table pagesize="15" class="displaytag" keepStatus="true"
                   name="assistants" requestURI="${requestURI}" id="row">
            <display:column>
                <a href="user/viewRendez.do?userId=${row.id}&rendezvousId=${idR}"> <spring:message
                        code="user.view"/>
                </a>
            </display:column>
        <spring:message code="user.name" var="name"/>
        <display:column property="name" title="${name}" sortable="true"/>
        <spring:message code="user.surname" var="surname"/>
        <display:column property="surname" title="${surname}" sortable="true"/>
        <spring:message code="user.email" var="email"/>
        <display:column property="email" title="${email}" sortable="true"/>
        <spring:message code="user.phone" var="phoneNumber"/>
        <display:column property="phoneNumber" title="${phoneNumber}" sortable="true"/>
        <spring:message code="user.address" var="address"/>
        <display:column property="address" title="${address}" sortable="true"/>
    </display:table>

</div>