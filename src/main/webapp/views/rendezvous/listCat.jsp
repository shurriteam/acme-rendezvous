<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>



<c:forEach items="${categories}" var="cat">


    <h3><jstl:out value="${cat.name}"/></h3>
    <p><jstl:out value="${cat.description}"/></p>

    <display:table pagesize="15" class="displaytag" keepStatus="true"
                   name="${cat.associatedServices}" requestURI="${requestURI}" id="row">


        <spring:message code="servicee.name" var="name"/>
        <display:column property="name" title="${name}" sortable="true"/>

        <spring:message code="servicee.description" var="description"/>
        <display:column property="description" title="${description}" sortable="true"/>

        <spring:message code="servicee.url" var="url"/>
        <display:column property="url" title="${url}" sortable="true"/>

    </display:table>

</c:forEach>





<!-- Listing grid -->
