<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<security:authorize access="hasRole('USER')">
    <a class="button2" href="rendezvous/create.do?"> <spring:message
            code="general.create"/>
    </a>
</security:authorize>
<!-- Listing grid -->
<display:table pagesize="15" class="displaytag" keepStatus="true"
               name="rendezvouses" requestURI="${requestURI}" id="row">

    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a href="rendezvous/delete.do?rendezvousId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>
    <security:authorize access="isAnonymous()">
        <display:column>
            <a href="user/view.do?userId=${row.owner.id}"> <spring:message
                    code="user.view"/>
            </a>
        </display:column>
    </security:authorize>
    <security:authorize access="hasRole('USER')">
        <%--<jstl:if test="${rendezStatus == 'DRAFT'}">--%>

        <jstl:if test="${my}">
        <display:column>
            <a href="rendezvous/edit.do?rendezvousId=${row.id}"> <spring:message
                    code="general.edit"/>
            </a>
        </display:column>
            <display:column>
                <a href="rendezvous/associate.do?rendezvousId=${row.id}"> <spring:message
                        code="user.associate.rendezvous"/>
                </a>
            </display:column>
            <display:column>
                <a href="announcement/create.do?rendezvousId=${row.id}"> <spring:message
                        code="user.announcement.create"/>
                </a>
            </display:column>
        </jstl:if>
        <%--</jstl:if>--%>

    </security:authorize>
    <spring:message code="rendezvous.rendezStatus" var="rendezStatus"/>
    <display:column property="rendezStatus" title="${rendezStatus}" sortable="true"/>

    <spring:message code="rendezvous.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>
    <spring:message code="rendezvous.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="rendezvous.picture" var="picture"/>
    <display:column title="${picture}">
        <img src="${row.picture}" width="130" height="100">
    </display:column>
    <spring:message code="rendezvous.publishMomment" var="publishMomment"/>
    <display:column property="publishMomment" title="${publishMomment}" sortable="true"/>

    <spring:message code="rendezvous.momment" var="organizedMomment"/>
    <display:column property="organizedMomment" title="${organizedMomment}" sortable="true"/>
    <spring:message code="rendezvous.owner" var="owner"/>
    <display:column property="owner" title="${owner}" sortable="true"/>

    <security:authorize access="hasRole('USER')">
        <%--TODO el my no va, si est�s en los rendeztoassist aparece el delete--%>
        <%--<jstl:if test="${rendezStatus == 'DRAFT'}">--%>
        <jstl:if test="${my}">
        <display:column>
            <a href="rendezvous/delete.do?rendezvousId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
        </jstl:if>
        <%--</jstl:if>--%>
    </security:authorize>

    <security:authorize access="isAnonymous()">
    <display:column>
        <a href="rendezvous/viewNotAuthenticated.do?rendezvousId=${row.id}"> <spring:message
                code="user.view.rendezvous"/>
        </a>
    </display:column>
    </security:authorize>

    <security:authorize access="isAuthenticated()">
        <display:column>
            <a href="rendezvous/view.do?rendezvousId=${row.id}"> <spring:message
                    code="user.view.rendezvous"/>
            </a>
        </display:column>
    </security:authorize>

    <security:authorize access="isAuthenticated()">
        <jstl:if test="${my}">
        <display:column>
            <a href="question/list.do?rendezvousId=${row.id}"> <spring:message
                    code="user.view.questions"/>
            </a>
        </display:column>

        <display:column>
            <a href="question/create.do?rendezvousId=${row.id}"> <spring:message
                    code="list.rendezvous.questions.add"/>
            </a>
        </display:column>

            <display:column>
                <a href="rendezvous/publish.do?rendezvousId=${row.id}"> <spring:message
                        code="list.rendezvous.publish"/>
                </a>
            </display:column>
        </jstl:if>
    </security:authorize>
    <%--<jstl:if test="${row.rendezStatus == 'DRAFT'}">--%>






</display:table>