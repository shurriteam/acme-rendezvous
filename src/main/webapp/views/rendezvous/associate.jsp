<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<form:form action="rendezvous/associate.do" modelAttribute="rendezvous">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="owner"/>
    <form:hidden path="assistants"/>
    <form:hidden path="related"/>
    <form:hidden path="announcements"/>
    <form:hidden path="comments"/>
    <form:hidden path="organizedMomment"/>
    <form:hidden path="name"/>
    <form:hidden path="description"/>
    <form:hidden path="picture"/>
    <form:hidden path="localization"/>


    <c:forEach items="${rendezvousesTo}" var="rndz">

        <input type="checkbox" name="rendezvous" value="${rndz.id}">${rndz.name}<br>

    </c:forEach>


    <%--<table>--%>
    <%--<td>Rendezvouses:</td>--%>

    <%--&lt;%&ndash; Approach 2: Property is of an array or of type java.util.Collection &ndash;%&gt;--%>

    <%--<c:choose>--%>
    <%--<c:when test="${not empty rendezvousesTo}">--%>
    <%--<c:forEach items="${rendezvousesTo}" var="rndz">--%>
    <%--<tr>--%>
    <%--<td><input name="name" value="${rendezvousesTo.name}" hidden></td>--%>
    <%--<td>${rndz.name}</td>--%>
    <%--<td><input type="checkbox" id="${rendezvousesTo.name}" value="${rendezvousesTo.name}"></td>--%>
    <%--</tr>--%>
    <%--</c:forEach>--%>
    <%--</c:when>--%>
    <%--</c:choose>--%>

    <%--</table>--%>

    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>


    <a class="button" href="/welcome/index.do"><spring:message code="general.cancel"/></a>


</form:form>