<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<form:form action="rendezvous/edit.do" modelAttribute="rendezvous">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="owner"/>
    <form:hidden path="assistants"/>
    <form:hidden path="related"/>
    <form:hidden path="announcements"/>
    <form:hidden path="comments"/>
    <form:hidden path="organizedMomment"/>


    <acme:textbox path="name" code="rendezvous.name"/>
    <acme:textarea path="description" code="rendezvous.description"/>
    <acme:textbox path="picture" code="rendezvous.picture"/>


    <acme:textbox path="localization.longuitide" code="rendezvous.coordinate.longuitide"/>
    <acme:textbox path="localization.longDirection" code="rendezvous.coordinate.longDirection"/>
    <acme:textbox path="localization.latitude" code="rendezvous.coordinate.latitude"/>
    <acme:textbox path="localization.latDirection" code="rendezvous.coordinate.latDirection"/>



    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>


    <a class="button" href="/welcome/index.do"><spring:message code="general.cancel"/></a>


</form:form>