<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 1/3/17
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<spring:message code="rendezvous.picture" var="picture1"/>
<h3><jstl:out value="${picture1}"/></h3>
<img src="<jstl:out value="${picture}"/>" width="500px" height="100%" alt="image_head">

<spring:message code="rendezvous.name" var="name1"/>
<h3><jstl:out value="${name1}"/></h3>
<jstl:out value="${name}"/>


<spring:message code="rendezvous.description" var="description1"/>
<h3><jstl:out value="${description1}"/></h3>
<jstl:out value="${description}"/>


<spring:message code="rendezvous.momment" var="momment1"/>
<h3><jstl:out value="${momment1}"/></h3>
<jstl:out value="${organizedMomment}"/>

<spring:message code="rendezvous.localization" var="localization1"/>
<h3><jstl:out value="${localization1}"/></h3>
<jstl:out value="${localization}"/>

<spring:message code="rendezvous.owner" var="owner1"/>
<h3><jstl:out value="${owner1}"/></h3>
<jstl:out value="${owner}"/>

<div id="comment-box">

<spring:message code="rendezvous.comments" var="comments2"/>
<h2><jstl:out value="${comments2}"/></h2>

<security:authorize access="hasRole('USER')">
    <%--Comprobamos si el manager es propiertario del viaje--%>

    <%--<a class="button2" href="/manageer/zustCreate.do?tripId=${tripid}"><spring:message code="trip.create.zust"/></a>--%>

</security:authorize>

<c:forEach items="${comments}" var="comment">

    <h2><jstl:out value="${comment.text}"/></h2>
    <h2><jstl:out value="${comment.description}"/></h2>
    <h2><jstl:out value="${comment.owner}"/></h2>


    </div>
</c:forEach>

<hr>


</div>


<div id="announcement-box">

<spring:message code="rendezvous.announcement" var="announcement1"/>
<h2><jstl:out value="${announcement1}"/></h2>


<c:forEach items="${announcements}" var="announcement">

    <h2><jstl:out value="${announcement.creationMomment}"/></h2>
    <h2><jstl:out value="${announcement.title}"/></h2>
    <h2><jstl:out value="${announcement.description}"/></h2>


    </div>
</c:forEach>


<hr>

</div>

<div id="realted-box">

<spring:message code="rendezvous.related" var="related1"/>
<h2><jstl:out value="${related1}"/></h2>


<c:forEach items="${related}" var="relate">

    <h2><jstl:out value="${relate.name}"/></h2>
    <h2><jstl:out value="${relate.description}"/></h2>
    <h2><jstl:out value="${relate.picture}"/></h2>


    </div>
</c:forEach>


<hr>
</div>

<div id="assis-box">

    <spring:message code="rendezvous.assistances" var="assistances1"/>
    <h2><jstl:out value="${assistances1}"/></h2>

    <display:table pagesize="15" class="displaytag" keepStatus="true"
                   name="assistants" requestURI="${requestURI}" id="row">


        <security:authorize access="isAnonymous()">
            <display:column>
                <a href="user/view.do?userId=${row.id}"> <spring:message
                        code="user.view"/>
                </a>
            </display:column>
        </security:authorize>
        <spring:message code="user.name" var="name"/>
        <display:column property="name" title="${name}" sortable="true"/>
        <spring:message code="user.surname" var="surname"/>
        <display:column property="surname" title="${surname}" sortable="true"/>
        <spring:message code="user.email" var="email"/>
        <display:column property="email" title="${email}" sortable="true"/>
        <spring:message code="user.phone" var="phoneNumber"/>
        <display:column property="phoneNumber" title="${phoneNumber}" sortable="true"/>
        <spring:message code="user.address" var="address"/>
        <display:column property="address" title="${address}" sortable="true"/>
    </display:table>


</div>