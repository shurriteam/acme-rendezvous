<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>


<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<!-- Listing grid -->
<display:table pagesize="15" class="displaytag" keepStatus="true"
               name="announcements" requestURI="${requestURI}" id="row">


    <spring:message code="rendezvous.name" var="title"/>
    <display:column property="title" title="${title}" sortable="true"/>
    <spring:message code="rendezvous.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>


    <spring:message code="rendezvous.momment" var="creationMomment"/>
    <display:column property="creationMomment" title="${creationMomment}" sortable="true"/>
    <spring:message code="announcement.rendezvous" var="rendezvous"/>
    <display:column property="rendezvous" title="${rendezvous}" sortable="true"/>


    <security:authorize access="hasRole('ADMINISTRATOR')">
        <display:column>
            <a href="announcement/delete.do?announcementId=${row.id}"> <spring:message
                    code="general.delete"/>
            </a>
        </display:column>
    </security:authorize>

    <%--<security:authorize access="hasRole('USER')">--%>
    <%--<display:column>--%>
    <%--<a href="rendezvous/delete.do?rendezvousId=${row.id}"> <spring:message--%>
    <%--code="general.delete"/>--%>
    <%--</a>--%>
    <%--</display:column>--%>
    <%--</security:authorize>--%>


    <%--<display:column>--%>
    <%--<a href="rendezvous/view.do?rendezvousId=${row.id}"> <spring:message--%>
    <%--code="user.view.rendezvous"/>--%>
    <%--</a>--%>
    <%--</display:column>--%>
    <%--<display:column>--%>
    <%--<a href="announcement/create.do?rendezvousId=${row.id}"> <spring:message--%>
    <%--code="user.create.announcement"/>--%>
    <%--</a>--%>
    <%--</display:column>--%>
</display:table>