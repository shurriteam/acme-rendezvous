<%--
  Created by IntelliJ IDEA.
  User: kawtarchbouki
  Date: 3/3/18
  Time: 17:19
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<security:authorize access="hasRole('MANAGER')">
    <a class="button2" href="servicee/create.do?"> <spring:message
            code="general.create"/>
    </a>
</security:authorize>




<display:table pagesize="15" class="displaytag" keepStatus="true"
               name="serviceesAll" requestURI="${requestURI}" id="row">


    <spring:message code="servicee.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>

    <spring:message code="servicee.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="servicee.url" var="url"/>
    <display:column property="url" title="${url}" sortable="true"/>
    <spring:message code="servicee.categorie" var="categorie"/>
    <display:column property="categorie" title="${categorie}" sortable="true"/>
    <security:authorize access="hasRole('ADMINISTRATOR')">


    </security:authorize>


</display:table>

<spring:message code="manager.servicees" var="servicees1"/>
<h2><jstl:out value="${servicees1}"/></h2>


<display:table pagesize="15" class="displaytag" keepStatus="true"
               name="servicees" requestURI="${requestURI}" id="row">
    <security:authorize access="hasRole('USER')">
        <display:column>
            <a href="requestForm/createForm.do?serviceeId=${row.id}"> <spring:message
                    code="service.request"/>
            </a>
        </display:column>
    </security:authorize>



    <security:authorize access="hasRole('MANAGER')">
        <display:column>
            <a href="servicee/edit.do?serviceeId=${row.id}"> <spring:message
                    code="general.edit"/>
            </a>
        </display:column>
    </security:authorize>

    <spring:message code="servicee.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>

    <spring:message code="servicee.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="servicee.url" var="url"/>
    <display:column property="url" title="${url}" sortable="true"/>
    <spring:message code="servicee.categorie" var="categorie"/>
    <display:column property="categorie" title="${categorie}" sortable="true"/>
    <security:authorize access="hasRole('ADMINISTRATOR')">


    </security:authorize>


</display:table>

