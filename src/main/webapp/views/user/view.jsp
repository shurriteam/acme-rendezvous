<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mruwzum
  Date: 1/3/17
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<spring:message code="user.name" var="name1"/>
<h3><jstl:out value="${name1}"/></h3>
<jstl:out value="${name}"/>


<spring:message code="user.surname" var="surname1"/>
<h3><jstl:out value="${surname1}"/></h3>
<jstl:out value="${surname}"/>


<spring:message code="user.email" var="email1"/>
<h3><jstl:out value="${email1}"/></h3>
<jstl:out value="${email}"/>

<spring:message code="user.phone" var="phoneNumber1"/>
<h3><jstl:out value="${phoneNumber1}"/></h3>
<jstl:out value="${phoneNumber}"/>


<spring:message code="user.address" var="address1"/>
<h3><jstl:out value="${address1}"/></h3>
<jstl:out value="${address}"/>


<jstl:if test="${not notForm}">
	<spring:message code="user.questions" var="questions1"/>
	<h3><jstl:out value="${questions1}"/> <jstl:out value="${renName}"/></h3>

    <c:forEach begin="0" end="${listSize}" varStatus="loop">


			<h3><jstl:out value="${questions[loop.index].text}"/></h3>
			<h3><jstl:out value="${answers[loop.index].text}"/></h3>

	</c:forEach>
</jstl:if>


<spring:message code="rendezvous.toGo" var="toGO"/>
<h2><jstl:out value="${toGO}"/></h2>

<display:table pagesize="15" class="displaytag" keepStatus="true"
               name="rendezToAssist" requestURI="${requestURI}" id="row">

    <security:authorize access="isAnonymous()">
        <display:column>
            <a href="user/view.do?userId=${row.owner.id}"> <spring:message
                    code="user.view"/>
            </a>
        </display:column>
    </security:authorize>
    <security:authorize access="hasRole('USER')">
    </security:authorize>
    <spring:message code="rendezvous.rendezStatus" var="rendezStatus"/>
    <display:column property="rendezStatus" title="${rendezStatus}" sortable="true"/>

    <spring:message code="rendezvous.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>
    <spring:message code="rendezvous.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="rendezvous.picture" var="picture"/>
    <display:column title="${picture}">
        <img src="${row.picture}" width="130" height="100">
    </display:column>


    <spring:message code="rendezvous.momment" var="organizedMomment"/>
    <display:column property="organizedMomment" title="${organizedMomment}" sortable="true"/>
    <spring:message code="rendezvous.owner" var="owner"/>
    <display:column property="owner" title="${owner}" sortable="true"/>

    <security:authorize access="isAnonymous()">
        <display:column>
            <a href="rendezvous/viewNotAuthenticated.do?rendezvousId=${row.id}"> <spring:message
                    code="user.view.rendezvous"/>
            </a>
        </display:column>
    </security:authorize>

    <security:authorize access="isAuthenticated()">
        <display:column>
            <a href="rendezvous/view.do?rendezvousId=${row.id}"> <spring:message
                    code="user.view.rendezvous"/>
            </a>
        </display:column>
    </security:authorize>

</display:table>


<spring:message code="endezvous.createdRendez" var="createdRende"/>
<h2><jstl:out value="${createdRende}"/></h2>
<display:table pagesize="15" class="displaytag" keepStatus="true"
               name="createdRendez" requestURI="${requestURI}" id="row">

    <security:authorize access="isAnonymous()">
        <display:column>
            <a href="user/view.do?userId=${row.owner.id}"> <spring:message
                    code="user.view"/>
            </a>
        </display:column>
    </security:authorize>
    <security:authorize access="hasRole('USER')">
    </security:authorize>
    <spring:message code="rendezvous.rendezStatus" var="rendezStatus"/>
    <display:column property="rendezStatus" title="${rendezStatus}" sortable="true"/>

    <spring:message code="rendezvous.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>
    <spring:message code="rendezvous.description" var="description"/>
    <display:column property="description" title="${description}" sortable="true"/>

    <spring:message code="rendezvous.picture" var="picture"/>
    <display:column title="${picture}">
        <img src="${row.picture}" width="130" height="100">
    </display:column>


    <spring:message code="rendezvous.momment" var="organizedMomment"/>
    <display:column property="organizedMomment" title="${organizedMomment}" sortable="true"/>
    <spring:message code="rendezvous.owner" var="owner"/>
    <display:column property="owner" title="${owner}" sortable="true"/>

    <security:authorize access="isAnonymous()">
        <display:column>
            <a href="rendezvous/viewNotAuthenticated.do?rendezvousId=${row.id}"> <spring:message
                    code="user.view.rendezvous"/>
            </a>
        </display:column>
    </security:authorize>

    <security:authorize access="isAuthenticated()">
        <display:column>
            <a href="rendezvous/view.do?rendezvousId=${row.id}"> <spring:message
                    code="user.view.rendezvous"/>
            </a>
        </display:column>
    </security:authorize>

</display:table>

<a class="button" href="/user/list.do"><spring:message code="general.cancel"/></a>
