<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>



<form:form action="servicee/edit.do" modelAttribute="servicee">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="owner"/>
    <form:hidden path="cancelled"/>
    <form:hidden path="rendezvous"/>


    <acme:textbox path="name" code="servicee.name"/>
    <acme:textbox path="description" code="servicee.description"/>
    <acme:textbox path="url" code="servicee.url"/>

    <acme:select path="categorie" code="servicee.category" items="${categoriezz}" itemLabel="name"/>
    <br/>



    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>
    <jstl:if test="${servicee.id ne 0}">
        <acme:submit name="delete" code="general.delete"/>
    </jstl:if>

    <acme:cancel code="general.cancel" url="/welcome/index.do"/>



</form:form>
    