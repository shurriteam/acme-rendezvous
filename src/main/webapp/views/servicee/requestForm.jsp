<%--
  Created by IntelliJ IDEA.
  User: kawtarchbouki
  Date: 12/3/18
  Time: 22:09
  To change this template use File | Settings | File Templates.
--%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form action="requestForm/edit.do" modelAttribute="requestForm">


    <form:hidden path="servicee" />
    <acme:select  path="rendezvous" code="requestForm.rendezvous" items="${userRendezvous}" itemLabel="name"/>
    <acme:textarea path="comment" code="requestForm.comment"/>




    <!---------------------------- BOTONES -------------------------->
    <input class="button" type="submit" name="request"
           value="<spring:message code="service.request" />"/>
    <button type="button" onclick="javascript: relativeRedir('/servicee/listAll.do')" >
    <spring:message code="general.cancel" /></button>

</form:form>

