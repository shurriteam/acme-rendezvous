<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
    <a href="welcome/index.do"><img src="images/logo.png" alt="Null Point Co., Inc." />
    </a>
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
        <security:authorize access="hasRole('ADMINISTRATOR')">
			<li><a class="fNiv"><spring:message	code="master.page.administrator" /></a>
				<ul>
					<li class="arrow"></li>
                    <li><a href="comment/list.do"><spring:message code="master.page.list.comments"/></a></li>
                    <li><a href="rendezvous/listAll.do"><spring:message code="master.page.rendezvous.list"/></a></li>
                    <li><a href="announcement/listAll.do"><spring:message code="master.page.announcements.list"/></a>
					<li><a href="servicee/listAll.do"><spring:message code="master.page.rendezvous.servicee"/></a>
					<li><a href="category/list.do"><spring:message code="master.page.category.list"/></a>

					<%--<li><a href="administrator/action-2.do"><spring:message code="master.page.administrator.action.2" /></a></li>					--%>
                </ul>
			</li>
            <li><a href="banner/list.do"><spring:message code="master.page.administrator.bannerlist" /></a></li>
            <li><a href="administrator/welcomePhraseEdit.do"><spring:message code="master.page.administrator.welcomePhrase" /></a></li>
            <li><a href="administrator/bussinesNameEdit.do"><spring:message code="master.page.administrator.bussinesName" /></a></li>
			<li><a href="administrator/dashboard.do"><spring:message code="master.page.dashboard"/></a></li>

		</security:authorize>

        <security:authorize access="hasRole('USER')">
			<li><a href="rendezvous/list.do"><spring:message code="master.page.rendezvous.list.my"/></a></li>
			<li><a href="rendezvous/listNotMy.do"><spring:message code="master.page.rendezvous.list"/></a></li>
            <li><a href="rendezvous/listToAssist.do"><spring:message code="master.page.rendezvous.listToAssist"/></a>
			<li><a href="user/listMy.do"><spring:message code="master.page.rendezvous.announcements"/></a>
			</li>
            <li><a href="announcement/listMyCreated.do"><spring:message code="master.page.announcements.list.my"/></a>
            </li>

			<li><a href="servicee/listAll.do"><spring:message code="master.page.rendezvous.servicee"/></a>
			</li>
            <li><a href="creditcard/edit.do"><spring:message code="master.page.edit.cc"/></a>
            </li>
            <%--<li><a class="fNiv"><spring:message	code="master.page.customer" /></a>--%>
            <%--<ul>--%>
            <%--<li class="arrow"></li>--%>

            <%--&lt;%&ndash;<li><a href="customer/action-2.do"><spring:message code="master.page.customer.action.2" /></a></li>					&ndash;%&gt;--%>
            <%--</ul>--%>
            <%--</li>--%>

		</security:authorize>


		<security:authorize access="hasRole('MANAGER')">
			<%--<li><a href="servicee/listToManager.do"><spring:message code="master.servicee.list.my"/></a>--%>
			<li><a href="managr/listServices.do"><spring:message code="master.page.rendezvous.servicee"/></a>

			</li>




		</security:authorize>

		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
            <li><a href="user/create.do"><spring:message code="master.page.user.register"/></a></li>
            <li><a href="user/list.do"><spring:message code="master.page.user.ListUsers"/></a></li>
            <li><a href="rendezvous/listAll.do"><spring:message code="master.page.rendezvous.list"/></a></li>
			<li><a href="category/listService.do"><spring:message code="master.page.service.list"/></a></li>
			<li><a href="managr/create.do"><spring:message code="master.page.manager.register"/></a></li>


		</security:authorize>
		
		<security:authorize access="isAuthenticated()">
			<li><a href="category/list.do"><spring:message code="master.page.category.list"/></a>

			<li>

                <a class="fNiv">
					<spring:message code="master.page.profile" /> 
			        (<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>
                        <%--<li><a href="profile/action-1.do"><spring:message code="master.page.profile.action.1" /></a></li>--%>
                        <%--<li><a href="profile/action-2.do"><spring:message code="master.page.profile.action.2" /></a></li>--%>
                        <%--<li><a href="profile/action-3.do"><spring:message code="master.page.profile.action.3" /></a></li>					--%>
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

