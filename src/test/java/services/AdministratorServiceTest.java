package services;

import domain.Announcement;
import domain.Comment;
import domain.Rendezvous;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import utilities.AbstractTest;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional


public class AdministratorServiceTest extends AbstractTest {

   @Autowired
   private CommentService commentService;
   @Autowired
   private RendezvousService rendezvousService;
   @Autowired
   private AnnouncementService announcementService;
   @Autowired
   private ActorService actorService;
   @Autowired
   private AdministratorService administratorService;


   //AE-177 C - An actor who is authenticated as an administrator must be able to: Remove a comment that he or she thinks is inappropriate.
   @Test
   public void removeAnInnapropiateCommentPositive() {
      authenticate("administrator1");
      List<Comment> comments = new ArrayList<>(commentService.findAll());
      int sizeBefore = comments.size();
      comments.remove(0);
      int sizeAfter = comments.size();
      Assert.assertNotEquals(sizeBefore, sizeAfter);
      unauthenticate();
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void removeAnInnapropiateCommentNegative() {
      authenticate("administrator1");
      List<Comment> comments = new ArrayList<>();
      int sizeBefore = comments.size();
      commentService.delete(comments.get(0));
      int sizeAfter = comments.size();
      Assert.assertNotEquals(sizeBefore, sizeAfter);
      unauthenticate();
   }

   //AE-177 C - An actor who is authenticated as an administrator must be able to: Remove a rendezvous that he or she thinks is inappropriate.
   @Test
   public void removeAnInnapropiateRendezvousPositive() {
      authenticate("administrator1");
      List<Rendezvous> rendezvouses = new ArrayList<>(rendezvousService.findAll());
      int sizeBefore = rendezvouses.size();
      rendezvouses.remove(0);
      int sizeAfter = rendezvouses.size();
      Assert.assertNotEquals(sizeBefore, sizeAfter);
      unauthenticate();
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void removeAnInnapropiateRendezvousNegative() {
      authenticate("administrator1");
      List<Rendezvous> rendezvouses = new ArrayList<>();
      int sizeBefore = rendezvouses.size();
      rendezvousService.delete(rendezvouses.get(0));
      int sizeAfter = rendezvouses.size();
      Assert.assertNotEquals(sizeBefore, sizeAfter);
      unauthenticate();
   }

   //AE-188 B - An actor who is authenticated as an administrator must be able to: Remove an announcement that he or she thinks is inappropriate.

   @Test
   public void removeAnInnapropiateAnnouncementPositive() {
      authenticate("administrator1");
      List<Announcement> announcements = new ArrayList<>(announcementService.findAll());
      int sizeBefore = announcements.size();
      announcements.remove(0);
      int sizeAfter = announcements.size();
      Assert.assertNotEquals(sizeBefore, sizeAfter);
      unauthenticate();
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void removeAnInnapropiateAnnouncementNegative() {
      authenticate("administrator1");
      List<Announcement> announcements = new ArrayList<>();
      int sizeBefore = announcements.size();
      announcementService.delete(announcements.get(0));
      int sizeAfter = announcements.size();
      Assert.assertNotEquals(sizeBefore, sizeAfter);
      unauthenticate();
   }

   //AE-177 C - An actor who is authenticated as an administrator must be able to: Remove a comment that he or she thinks is inappropriate.
   @Test
   public void removeAnInnapropiatedCommentPositive() {
      authenticate("administrator1");
      List<Comment> comments = new ArrayList<>(commentService.findAll());
      int sizeA = comments.size();
      comments.remove(0);
      int sizeB = comments.size();
      Assert.assertNotEquals(sizeA, sizeB);
      unauthenticate();
   }

   @Test(expected = IllegalArgumentException.class)
   public void removeAnInnapropiatedCommentNegative() {
      authenticate("administrator134325");
      List<Comment> comments = new ArrayList<>(commentService.findAll());
      int sizeA = comments.size();
      comments.remove(0);
      int sizeB = comments.size();
      Assert.assertNotEquals(sizeA, sizeB);
      unauthenticate();
   }







}