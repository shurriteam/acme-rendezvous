package services;

import domain.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class UserServiceTest extends AbstractTest {


   @Autowired
   private ActorService actorService;
   @Autowired
   private UserService userService;
   @Autowired
   private RendezvousService rendezvousService;
   @Autowired
   private AnnouncementService announcementService;
   @Autowired
   private CoordinateService coordinateService;
   @Autowired
   private FormService formService;
   @Autowired
   private AnswerService answerService;
   @Autowired
   private QuestionService questionService;


   // AE-166 C - An actor who is not authenticated must be able to: Register to the system as a user. Positive  & negative cases.
   @Test(expected = AssertionError.class)
   public void registerAsUserNegative() {
      User user = userService.create();
      user.setName("perri");
      user.setEmail("perri@gmail.com");
      user.setAddress("calle de perri 23");
      user.setPhoneNumber("+34567345234");
      user.setSurname("el perro");
      UserAccount userAcco = new UserAccount();
      userAcco.setPassword("passwd");
      userAcco.setUsername("perriel");
      Set<Authority> auth = new HashSet<>();
      Authority authority = new Authority();
      authority.setAuthority("EXPLORER");
      auth.add(authority);
      userAcco.setAuthorities(auth);
      user.setUserAccount(userAcco);

      Actor res = userService.registerAsUser(user);
      Assert.assertNotNull("resultado nulo al registrar el user", res);
   }

   @Test
   public void registerAsUserPositive() {
      User user0 = userService.create();
      user0.setName("perri");
      user0.setEmail("perri@gmail.com");
      user0.setAddress("calle de perri 23");
      user0.setPhoneNumber("+34567345234");
      user0.setSurname("el perro");
      UserAccount userAcco = new UserAccount();
      userAcco.setPassword("passwd");
      userAcco.setUsername("perriel");
      Set<Authority> auth = new HashSet<>();
      Authority authority = new Authority();
      authority.setAuthority("EXPLORER");
      auth.add(authority);
      userAcco.setAuthorities(auth);
      user0.setUserAccount(userAcco);
      Actor res = userService.registerAsUser(user0);

   }

   // AE-166 C - An actor who is not authenticated must be able to: List the users of the system and navigate to their profiles

   @Test
   public void listUserSystemsNoAuthPositive() {
      authenticate(null);
      Collection<User> users = userService.findAll();
      org.springframework.util.Assert.notEmpty(users, "no users listed");
      unauthenticate();
   }

   @Test(expected = IllegalArgumentException.class)
   public void listUserSystemsNoAuthNegative() {
      authenticate(null);
      Collection<User> users = new ArrayList<>();
      org.springframework.util.Assert.notEmpty(users, "no users listed");
      unauthenticate();
   }
   //AE-166 C - An actor who is not authenticated must be able to: List the rendezvouses in the system and navigate to the profiles

   @Test
   public void listRendezAndGoToUsersProfilePositive() {
      authenticate(null);
      List<Rendezvous> rendezvouses = new ArrayList<>(rendezvousService.findAll());
      Rendezvous toViewUsersProfile = rendezvouses.get(0);
      User ownerOfRendez = toViewUsersProfile.getOwner();
      org.springframework.util.Assert.notNull(ownerOfRendez.getName());
      org.springframework.util.Assert.notNull(ownerOfRendez.getEmail());
      org.springframework.util.Assert.notNull(ownerOfRendez.getAddress());
      org.springframework.util.Assert.notNull(ownerOfRendez.getPhoneNumber());
      org.springframework.util.Assert.notNull(ownerOfRendez.getSurname());
      unauthenticate();
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void listRendezAndGoToUsersProfileNegative() {
      authenticate(null);
      List<Rendezvous> rendezvouses = new ArrayList<>();
      Rendezvous toViewUsersProfile = rendezvouses.get(0);
      User ownerOfRendez = toViewUsersProfile.getOwner();
      org.springframework.util.Assert.notNull(ownerOfRendez.getName());
      org.springframework.util.Assert.notNull(ownerOfRendez.getEmail());
      org.springframework.util.Assert.notNull(ownerOfRendez.getAddress());
      org.springframework.util.Assert.notNull(ownerOfRendez.getPhoneNumber());
      org.springframework.util.Assert.notNull(ownerOfRendez.getSurname());
      unauthenticate();
   }

   //AE-181 B - An actor who is not authenticated must be able to: List the announcements that are associated with each rendezvous.
   @Test(expected = IllegalArgumentException.class)
   public void listAnnouncementsPositive() {
      authenticate(null);
      List<Rendezvous> rendezvouses = new ArrayList<>(rendezvousService.findAll());
      Rendezvous rendezvous1 = rendezvouses.get(3);
      Collection<Announcement> announcements = rendezvous1.getAnnouncements();
      org.springframework.util.Assert.notEmpty(announcements, "Empty announcements or null rendezvous");
      unauthenticate();
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void listAnnouncementsNegative() {
      authenticate(null);
      List<Rendezvous> rendezvouses = new ArrayList<>(rendezvousService.findAll());
      Rendezvous rendezvous1 = rendezvouses.get(20);
      Collection<Announcement> announcements = rendezvous1.getAnnouncements();
      org.springframework.util.Assert.notEmpty(announcements, "Empty announcements or null rendezvous");
      unauthenticate();
   }

   // AE-181 B - An actor who is not authenticated must be able to: Navigate from a rendezvous to the rendezvouses that are similar to it.

   @Test
   public void listRelatedPositive() {
      authenticate(null);
      List<Rendezvous> rendezvouses = new ArrayList<>(rendezvousService.findAll());
      Rendezvous rendezvous1 = rendezvouses.get(3);
      Collection<Rendezvous> related = rendezvous1.getRelated();
      org.springframework.util.Assert.notEmpty(related, "Empty related or null rendezvous");
      unauthenticate();
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void listRelatedNegative() {
      authenticate(null);
      List<Rendezvous> rendezvouses = new ArrayList<>();
      Rendezvous rendezvous1 = rendezvouses.get(0);
      Collection<Rendezvous> related = rendezvous1.getRelated();
      org.springframework.util.Assert.notEmpty(related, "Empty related or null rendezvous");
      unauthenticate();
   }

   //AE-184 B - An actor who is authenticated as a user must be able to: Create an announcement regarding one of the rendezvouses that he or she?s created previously.


   @Test
   public void createAnnouncementOnRendezPositive() {
      authenticate("user2");
      List<Rendezvous> myRendez = new ArrayList<>(userService.createdRenezByUser(actorService.findByPrincipal().getId()));
      Rendezvous rendezvous = myRendez.get(0);
      Announcement announcement = announcementService.create();
      announcement.setRendezvous(rendezvous);
      announcement.setDescription("description");
      announcement.setCreationMomment(new Date(System.currentTimeMillis() - 100));
      announcement.setTitle("title");
      Announcement announcement1 = announcementService.save(announcement);
      org.springframework.util.Assert.notNull(announcement1);
      unauthenticate();

   }

   @Test(expected = IllegalArgumentException.class)
   public void createAnnouncementOnRendezNegative() {
      authenticate(null);
      List<Rendezvous> myRendez = new ArrayList<>(userService.createdRenezByUser(actorService.findByPrincipal().getId()));
      Rendezvous rendezvous = myRendez.get(0);
      Announcement announcement = announcementService.create();
      announcement.setRendezvous(rendezvous);
      announcement.setDescription("description");
      announcement.setCreationMomment(new Date(System.currentTimeMillis() - 100));
      announcement.setTitle("title");
      Announcement announcement1 = announcementService.save(announcement);
      org.springframework.util.Assert.notNull(announcement1);
      unauthenticate();
   }

//AE-184 B - An actor who is authenticated as a user must be able to: Link one of the rendezvouses that he or she?s created to other similar rendezvouses.


   @Test
   public void linkRendezPosit() {
      authenticate("user2");
      List<Rendezvous> myRendez = new ArrayList<>(userService.createdRenezByUser(actorService.findByPrincipal().getId()));
      Rendezvous rendezvous = myRendez.get(0);
      List<Rendezvous> allRendez = new ArrayList<>(rendezvousService.findAll());
      int sizeBefore = rendezvous.getRelated().size();
      rendezvous.getRelated().add(allRendez.get(0));
      Assert.assertNotEquals(sizeBefore, rendezvous.getRelated().size());
      unauthenticate();

   }

   @Test(expected = IllegalArgumentException.class)
   public void linkRendeznegat() {
      authenticate(null);
      List<Rendezvous> myRendez = new ArrayList<>(userService.createdRenezByUser(actorService.findByPrincipal().getId()));
      Rendezvous rendezvous = myRendez.get(0);
      List<Rendezvous> allRendez = new ArrayList<>(rendezvousService.findAll());
      int sizeBefore = rendezvous.getRelated().size();
      rendezvous.getRelated().add(allRendez.get(0));
      Assert.assertNotEquals(sizeBefore, rendezvous.getRelated().size());

      unauthenticate();
   }
   //AE-184 B - An actor who is authenticated as a user must be able to: Display a stream of announcements that have been posted

   @Test(expected = IllegalArgumentException.class)
   public void streamOfpostedAnnouncementsPositive() {
      authenticate("user1");
      Collection<Announcement> announcement;
      announcement = userService.createdAnnouncementsByUser(userService.findByPrincipal().getId());
      org.springframework.util.Assert.notEmpty(announcement, "Empty announcement list");
      unauthenticate();

   }


   @Test(expected = IllegalArgumentException.class)
   public void streamOfpostedAnnouncementsNegative() {
      authenticate("perri");
      Collection<Announcement> announcement;
      announcement = userService.createdAnnouncementsByUser(userService.findByPrincipal().getId());
      org.springframework.util.Assert.notEmpty(announcement, "Empty announcement list");
      unauthenticate();
   }
   //AE-184 B - An actor who is authenticated as a user must be able to: Create a rendezvous which is implicit to asssist

   @Test
   public void createRendezvousPositive() {
      authenticate("user1");
      Rendezvous rendezvous = rendezvousService.create();
      rendezvous.setName("name");
      Coordinate coordinate = coordinateService.create();
      coordinate.setLatDirection("O");
      coordinate.setLongDirection("E");
      coordinate.setLatitude(124.5);
      coordinate.setLonguitide(-124.5);
      rendezvous.setLocalization(coordinate);
      Coordinate aux = coordinateService.save(rendezvous.getLocalization());
      userService.flush();
      rendezvous.setOwner(userService.findByPrincipal());
      rendezvous.setLocalization(aux);
      rendezvous.setOrganizedMomment(new Date(System.currentTimeMillis() - 100));
      rendezvous.setAdult(false);
      rendezvous.setDescription("asfdasfsdfasdfasdfasdfasdfsadfasdfasdfdsfasd");
      rendezvous.setPicture("http://newrednez.com/picture5.jpg");
      Rendezvous rendezvous1 = rendezvousService.save(rendezvous);
      rendezvousService.flush();
      rendezvousService.save(rendezvous1);
      Rendezvous rendezvous2 = rendezvousService.save(rendezvous1);
      userService.findByPrincipal().getCreatedRendez().add(rendezvous2);
      unauthenticate();

   }


   @Test(expected = NullPointerException.class)
   public void createRendezvousNegative() {
      authenticate("user1");
      Rendezvous rendezvous = rendezvousService.create();
      rendezvous.setName("name");
      Coordinate coordinate = coordinateService.create();
      coordinate.setLatDirection("O");
      coordinate.setLongDirection("E");
      coordinate.setLatitude(124.5);
      coordinate.setLonguitide(-124.5);
      rendezvous.setLocalization(coordinate);
      Coordinate aux = coordinateService.save(rendezvous.getLocalization());
      userService.flush();
      rendezvous.setOwner(userService.findByPrincipal());
      rendezvous.setLocalization(aux);
      rendezvous.setOrganizedMomment(new Date(System.currentTimeMillis() - 100));
      rendezvous.setAdult(false);
      rendezvous.setDescription("asfdasfsdfasdfasdfasdfasdfsadfasdfasdfdsfasd");
      rendezvous.setPicture("http://newrednez.com/picture5.jpg");
      Rendezvous rendezvous1 = rendezvousService.save(rendezvous);
      rendezvousService.flush();
      rendezvousService.save(rendezvous1);
      Rendezvous rendezvous2 = rendezvousService.save(rendezvous1);
      rendezvous2.getAssistants().add(userService.findByPrincipal());
      userService.findByPrincipal().getCreatedRendez().add(rendezvous2);

      unauthenticate();

   }

   //AE-170 C - An actor who is authenticated as a user must be able to: List the rendezvouses that he or she?s RSVPd

   @Test
   public void listRendezvousesRSVPdPositive() {
      authenticate("user2");
      Collection<Rendezvous> toAsist = userService.findByPrincipal().getRendezToAssist();
      org.springframework.util.Assert.notEmpty(toAsist);
      unauthenticate();
   }

   @Test(expected = IllegalArgumentException.class)
   public void listRendezvousesRSVPdNegative() {
      authenticate("perrrrrrii");
      Collection<Rendezvous> toAsist = userService.findByPrincipal().getRendezToAssist();
      org.springframework.util.Assert.notEmpty(toAsist);
      unauthenticate();
   }

   //AE-190 A - An actor who is not authenticated must be able to: Display information about the users who have RSVPd a rendezvous
   @Test
   public void displayInfoAboutUsersWhoHaveRSVPdARendezPositive() {
      authenticate(null);
      List<Rendezvous> all = new ArrayList<>(rendezvousService.findAll());
      User user = all.get(0).getOwner();
      org.springframework.util.Assert.notNull(user.getName());
      unauthenticate();
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void displayInfoAboutUsersWhoHaveRSVPdARendezNegative() {
      authenticate(null);
      List<Rendezvous> all = new ArrayList<>();
      User user = all.get(0).getOwner();
      org.springframework.util.Assert.notNull(user.getName());
      unauthenticate();
   }


   //AE-192 A - An actor who is authenticated as a user must be able to: Answer the questions that are associated with a rendezvous that he or she?s RSVPing now.


   @Test
   public void answerQuestionsOfARendezToRSVPPositive() {
      authenticate("user1");
      List<Rendezvous> rendezvouses = new ArrayList<>(userService.findByPrincipal().getRendezToAssist());
      Rendezvous rendezvous1 = rendezvouses.get(0);
      List<Question> rendezQuestions = new ArrayList<>(rendezvous1.getQuestions());
      Answer answer = answerService.create();
      answer.setText("dfgsdfdsa");
      answerService.save(answer);
      unauthenticate();
   }

   @Test(expected = IndexOutOfBoundsException.class)
   public void answerQuestionsOfARendezToRSVPNegative() {
      authenticate("user2");
      List<Rendezvous> rendezvouses = new ArrayList<>();
      Rendezvous rendezvous1 = rendezvouses.get(0);
      List<Question> rendezQuestions = new ArrayList<>(rendezvous1.getQuestions());
      Question question = rendezQuestions.get(0);
      Answer answer = answerService.create();
      answer.setText("dfgsdfdsa");
      answerService.save(answer);
      unauthenticate();
   }
//AE-192 A - An actor who is authenticated as a user must be able to: Manage the questions that are associated with a rendezvous that he or she?s created previously.

   @Test
   public void manageQuestionAssociatedToCreatedRendezvousesPositive() {
      authenticate("user1");
      List<Rendezvous> rendezvouses = new ArrayList<>(userService.findByPrincipal().getCreatedRendez());
      Rendezvous rendezvous1 = rendezvouses.get(0);
      List<Question> rendezQuestions = new ArrayList<>(rendezvous1.getQuestions());
      Question question = questionService.create();
      question.setText("dfgsdfdsa");
      question.setRendezvous(rendezvous1);
      questionService.save(question);
      unauthenticate();
   }

   @Test(expected = IllegalArgumentException.class)
   public void manageQuestionAssociatedToCreatedRendezvousesNegative() {
      authenticate(null);
      List<Rendezvous> rendezvouses = new ArrayList<>(userService.findByPrincipal().getCreatedRendez());
      Rendezvous rendezvous1 = rendezvouses.get(0);
      List<Question> rendezQuestions = new ArrayList<>(rendezvous1.getQuestions());
      Question question = questionService.create();
      question.setText("dfgsdfdsa");
      question.setRendezvous(rendezvous1);
      questionService.save(question);
      unauthenticate();
   }


   //AE-170 C - An actor who is authenticated as a user must be able to: RSVP a rendezvous or cancel it.


   @Test
   public void RSVPaRendezvousPositive() {
      authenticate("user3");
      List<Rendezvous> allRendez = new ArrayList<>(rendezvousService.findAll());
      List<Rendezvous> mine = new ArrayList<>(userService.findByPrincipal().getCreatedRendez());
      List<Rendezvous> toAssist = new ArrayList<>(userService.findByPrincipal().getRendezToAssist());
      allRendez.removeAll(mine);
      allRendez.removeAll(toAssist);
      Rendezvous toGo = allRendez.get(0);
      Collection<Question> questions = toGo.getQuestions();
      Assert.assertNotNull(questions);
      toGo.getAssistants().add(userService.findByPrincipal());
      unauthenticate();
   }


   @Test(expected = IllegalArgumentException.class)
   public void RSVPaRendezvousNegative() {
      authenticate("perrielperro");
      List<Rendezvous> allRendez = new ArrayList<>(rendezvousService.findAll());
      List<Rendezvous> mine = new ArrayList<>(userService.findByPrincipal().getCreatedRendez());
      List<Rendezvous> toAssist = new ArrayList<>(userService.findByPrincipal().getRendezToAssist());
      allRendez.removeAll(mine);
      allRendez.removeAll(toAssist);
      Rendezvous toGo = allRendez.get(0);
      Collection<Question> questions = toGo.getQuestions();
      Assert.assertNotNull(questions);
      toGo.getAssistants().add(userService.findByPrincipal());
      unauthenticate();
   }


   //AcmeAE-199 Display a stream of announcements that have been posted to the rendezvouses that he or she?s RSVPd. The announcements must be listed chronologically in descending order.

   @Test
   public void displayAnnouncementsPostedOnRendezRSVPD() {
      authenticate("user2");
      User user = userService.findByPrincipal();
      List<Rendezvous> rendezvousesToAssist = new ArrayList<>(user.getRendezToAssist());
      Rendezvous rendezvous = rendezvousesToAssist.get(0);
      List<Announcement> announcements = new ArrayList<>(rendezvous.getAnnouncements());
      org.springframework.util.Assert.notEmpty(announcements);
      unauthenticate();
   }

   @Test(expected = IllegalArgumentException.class)
   public void displayAnnouncementsPostedOnRendezRSVPDNegative() {
      authenticate("user2333");
      User user = userService.findByPrincipal();
      List<Rendezvous> rendezvousesToAssist = new ArrayList<>(user.getRendezToAssist());
      Rendezvous rendezvous = rendezvousesToAssist.get(0);
      List<Announcement> announcements = new ArrayList<>(rendezvous.getAnnouncements());
      org.springframework.util.Assert.notEmpty(announcements);
      unauthenticate();
   }

   //AE-200 Display information about the users who have RSVPd a rendezvous, which, in turn, must show their answers to the questions that the creator has registered.


   @Test
   public void infoAboutRSVPdUsersOfRendez() {
      authenticate("user2");
      User user = userService.findByPrincipal();
      List<Rendezvous> myRendez = new ArrayList<>(user.getCreatedRendez());
      Rendezvous rendezvous = myRendez.get(0);
      List<User> assistants = new ArrayList<>(rendezvous.getAssistants());
      List<Question> questionsOfRendez = new ArrayList<>(rendezvous.getQuestions());
      Question question = questionsOfRendez.get(0);
      unauthenticate();
   }

   @Test(expected = IllegalArgumentException.class)
   public void infoAboutRSVPdUsersOfRendezNega() {
      authenticate("user2erwe");
      User user = userService.findByPrincipal();
      List<Rendezvous> myRendez = new ArrayList<>(user.getCreatedRendez());
      Rendezvous rendezvous = myRendez.get(0);
      List<User> assistants = new ArrayList<>(rendezvous.getAssistants());
      List<Question> questionsOfRendez = new ArrayList<>(rendezvous.getQuestions());
      Question question = questionsOfRendez.get(0);
      unauthenticate();
   }
}