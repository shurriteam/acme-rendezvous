package services;
import domain.*;
import org.hibernate.criterion.IlikeExpression;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import security.Authority;
import security.UserAccount;
import security.UserAccountService;
import utilities.AbstractTest;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/datasource.xml",
        "classpath:spring/config/packages.xml"})
@Transactional
public class ExtraServiceTest extends AbstractTest {


   @Autowired
   private ActorService actorService;
   @Autowired
   private UserService userService;
   @Autowired
   private RendezvousService rendezvousService;
   @Autowired
   private AnnouncementService announcementService;
   @Autowired
   private CoordinateService coordinateService;
   @Autowired
   private FormService formService;
   @Autowired
   private AnswerService answerService;
   @Autowired
   private QuestionService questionService;
   @Autowired
   private ManagerService managerService;
   @Autowired
   private ServiceeService serviceeService;
   @Autowired
   private UserAccountService userAccountService;
   @Autowired
   private CategoryService categoryService;
   @Autowired
   private AdministratorService administratorService;
   @Autowired
   private CreditCardService creditCardService;
   @Autowired
   private RequestFormService requestFormService;

//a) you must select a use case that involves a listing and an edition requirement;
// for that use case, you must implement at least 10 test cases that provide a good enough coverage
// of your code and your data. b) For the remaining use cases, you must implement at least one positive
// test case and one negative test case.

/*

 @Test
   	public void samplePositiveTest() {
   		Assert.isTrue(true);
   	}

   	@Test(expected = IllegalArgumentException.class)
   	public void sampleNegativeTest() {
   		Assert.isTrue(false);
   	}

   	@Test
   	public void sampleDriver() {
   		final Object testingData[][] = {
   			{
   				"userAccount1", 4, null
   			}, {
   				"userAccount2", 5, null
   			}, {
   				"userAccount3", 6, null
   			}, {
   				"non-existent", 0, AssertionError.class
   			}
   		};

   		for (int i = 0; i < testingData.length; i++)
   			this.sampleTemplate((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
   	}

   	// Ancillary methods ------------------------------------------------------

   	protected void sampleTemplate(final String beanName, final int id, final Class<?> expected) {
   		Class<?> caught;
   		int dbId;

   		caught = null;
   		try {
   			dbId = super.getEntityId(beanName);
   			org.springframework.util.Assert.isTrue(dbId == id);
   		} catch (final Throwable oops) {
   			caught = oops.getClass();
   		}

   		this.checkExceptions(expected, caught);
   	}

   }


   */


   //---------------------------------


   // Tarea	AE-218
   //Services belong to categories, which may be organised into arbitrary hierarchies. A category is characterised by a name and a description.
// kawtar(hexo)
   @Test
   public void CreateCategory() {
       final Object testingData[][] = {
               {
                       "administrator1", "category1","mi primera categoria" ,null
               },
               {
                       "administrator2", "category2","mi segunda category" ,null
               },

               {
                       "administrator3", "category3","mi tercera category" ,null
               },

               {
                       "manager1", "category1","mi primera category" ,IllegalArgumentException.class
               },

               {
                       null, "category1","mi primera category" ,IllegalArgumentException.class
               },

               {
                       null, null,"mi primera category" ,IllegalArgumentException.class
               },
               {
                       "administrator1", null,null ,ConstraintViolationException.class
               },

       };

       for (int i = 0; i < testingData.length; i++)
           this.createCategoryTemplate((String) testingData[i][0],(String) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
   }


    // Ancillary methods ------------------------------------------------------

    protected void createCategoryTemplate(String beanName,String name, String description,final Class<?> expected) {
        Class<?> caught;
        caught = null;
        try {
            authenticate(beanName);
            Category category = categoryService.create();
            category.setDescription(description);
            category.setName(name);
            categoryService.save(category);
            categoryService.flush();

            Assert.notNull(category);

            unauthenticate();
        } catch (final Throwable oops) {
            caught = oops.getClass();
        }

        this.checkExceptions(expected, caught);
    }




    // Tarea	AE-217
   //Managers manage services, for which the system must store a name, a description, and an optional picture.--> hexo kaw
   @Test
   public void managerServicesManageDriver() {
      final Object testingData[][] = {
              {
                      "manager2", "adsfadsf", "fsadfsdaf", null
              },
              {
                      "manager1", "aaaaaa", "aaaaaa", null
              },
              {
                      "manager2", "sssss", "ssssss", null
              },
              {
                      "manager1", "sddddd", "asddddddddfsdffds", null
              },
              {
                      "manager2", "namdsffdse1", "asdfsdffds", null
              },
              {
                      "manager3", "ddsdas", "asdfsdffds", null
              },
              {
                      "manager3", null, "descripfsdfsdftion1", null
              },
              {
                      "manager4", null, "sdfdsfsdf", IllegalArgumentException.class
              },
              {
                     "user1", null, null, IllegalArgumentException.class
              },
              {
                      "manager2w", "name2", null, IllegalArgumentException.class
              },
              {      "userAccount3", null, "description2", IllegalArgumentException.class

              },
              {      "administrator1", null, "sdafsdfsda", IllegalArgumentException.class
              }
      };

      for (int i = 0; i < testingData.length; i++)
         this.managerServicesManageTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
   }


   // Ancillary methods ------------------------------------------------------

   protected void managerServicesManageTemplate(final String beanName, final String name, final String description,final Class<?> expected) {
      Class<?> caught;
      caught = null;
      try {
         authenticate(beanName);
         Manager manager = managerService.findByPrincipal();
         Servicee servicee = serviceeService.create();
         servicee.setName(name);
         servicee.setDescription(description);
         servicee.setOwner(manager);
         servicee.setUrl("http://www.genericPerril.net/posts/");
         serviceeService.save(servicee);

         Assert.notNull(servicee);

      } catch (final Throwable oops) {
         caught = oops.getClass();
      }

      this.checkExceptions(expected, caught);
   }



   // Tarea	AE-216
   //There?s a new kind of actor in the system: managers, who have a VAT number that is a string of letters, numbers, and dashes.
//kawtar: hexo kawww

    public void managerRegisterTest( String username,  String password,  String name,  String surname, String vat,  String phoneNumber,  String email,  String address,Integer age,  Class<?> expected) {
        Class<?> caught = null;

        try {

             Manager result = this.managerService.create();
            Set<Authority> auth = new HashSet<>();
            Authority authority = new Authority();
            UserAccount userAcco = new UserAccount();
            authority.setAuthority("MANAGER");
            auth.add(authority);
            userAcco.setAuthorities(auth);
            result.setUserAccount(userAcco);
            result.getUserAccount().setUsername(username);
            result.setName(name);
            result.setSurname(surname);
            result.setVAT(vat);
            result.setEmail(email);
            result.setAddress(address);
            result.getUserAccount().setPassword(new Md5PasswordEncoder().encodePassword(password, null));
            result.setAge(age);
            result.setPhoneNumber(phoneNumber);

            this.managerService.save(result);
            managerService.flush();


        } catch (final Throwable oops) {

            caught = oops.getClass();

        }

        this.checkExceptions(expected, caught);
    }
    @Test
    public void driverManagerRegisterTest() {

        final Object testingData[][] = {
                //TODO aqui cuando pongo casos de uso negativos me salen errores raros
                // Alguien sin registrar/logueado -> true
                //String username,  String password,  String name,  String surname, String vat,  String phoneNumber,  String email,  String address,int age
                {
                       "manager33", "manager33", "managerTestName", "managerTestSurname","123-ASD", "+34 123456789", "managerTest@hotmail.com", "addressTest",19, null
                },
                {
                        "manager22", "manager22", "managerTestName1", "managerTestSurname1","333-ADD", "+34 666306363", "managerTest@hotmail.com", "addressTest",33, null
                },
                {
                        "manager10", "manager10", "managerTestName2", "managerTestSurname3","222-ADD", "+34 666666666", "managerTest@hotmail.com", "addressTest",44, null
                },
                {
                        "manager33", "manager33", "managerTestName2", "managerTestSurname","123-ASD", "+34 123456789", "managerTest@hotmail.com", "addressTest",null
                        , NullPointerException.class
                },
                {
                        null, null, null, null, null, null, null, null, null,NullPointerException.class
                },
                {
                        "manager55", "manager55", "managerName44", "managerTestSurname","333-ASD", "", "managerTest@hotmail.com", "addressTest",19,null
                },
                /*
                {
                        "manager55", "manager55", "managerName44", "managerTestSurname","333-ASD", ""," ", "addressTest",19,NullPointerException.class
                },

              /*  {
                        "manager66", "manager66", "managerName66", "managerTestSurname","333-ASD", "", "", "addressTest",21,ConstraintViolationException.class
                },

                {
                        "manager44", "manager44", "managerName44", "managerTestSurname","333-ASD", "", "managerTest@hotmail.com", "addressTest",19,null

                },


*/




        };
        for (int i = 0; i < testingData.length; i++)
            this.managerRegisterTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
                    (String) testingData[i][7],(Integer) testingData[i][8], (Class<?>) testingData[i][9]);
    }


   // Tarea	AE-209
   //List the rendezvouses in the system grouped by category
   @Test
   public void listRendezvousesManageDriver() {
      final Object testingData[][] = {
              {
                      "user2", "adsfadsf", "fsadfsdaf", null
              },
              {
                      "user1", "aaaaaa", "aaaaaa", null
              },
              {
                      "user3", "sssss", "ssssss", null
              },
              {
                      "user4", "sddddd", "asddddddddfsdffds", null
              },
              {
                      "user2", "namdsffdse1", "asdfsdffds", null
              },
              {
                      "user1", "ddsdas", "asdfsdffds", null
              },
              {
                      "user2", null, "descripfsdfsdftion1", null
              },
              {
                      "manager4", null, "sdfdsfsdf", IllegalArgumentException.class
              },
              {
                      "user14", null, null, IllegalArgumentException.class
              },
              {
                      "manager2w", "name2", null, IllegalArgumentException.class
              },
              {      "userAccount3", null, "description2", IllegalArgumentException.class

              },
              {      "administrator1", null, "sdafsdfsda", IllegalArgumentException.class
              }
      };

      for (int i = 0; i < testingData.length; i++)
         this.listRendezvousesManageTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
   }


   // Ancillary methods ------------------------------------------------------

   protected void listRendezvousesManageTemplate(final String beanName, final String name, final String description,final Class<?> expected) {
      Class<?> caught;
      caught = null;
      try {
         authenticate(beanName);
         User manager = userService.findByPrincipal();
         List<Rendezvous> rendezvousList = new ArrayList<>( rendezvousService.findAll());
         Rendezvous rendezvous = rendezvousList.get(0);
         rendezvous.setName(name);
         rendezvous.setDescription(description);
         rendezvous.setOwner(manager);
         rendezvousService.save(rendezvous);
         Assert.notEmpty(rendezvousList);

      } catch (final Throwable oops) {
         caught = oops.getClass();
      }

      this.checkExceptions(expected, caught);
   }

   // Tarea	AE-207
   //Cancel a service that he or she finds inappropriate. Such services cannot be requested for any rendezvous. They must be flagged appropriately when listed.
// kawtar hexo


    @Test
    public void AdministratorDeleteServicesDriver() {
        final Object testingData[][] = {
                {

                        "administrator1", "servicee2", null
                },
                {
                        "administrator2", "servicee2", null
                },
                 {
                         "administrator3", "servicee3", null
                 },
                {
                        "manager1", "servicee1", IllegalArgumentException.class               },
                {
                        "manager2", "servicee2", IllegalArgumentException.class            },

                {
                        "manager1", null, AssertionError.class            },
                {
                        null, "servicee1",IllegalArgumentException.class

                },

                {
                        null, null,AssertionError.class

                },



        };

        for (int i = 0; i < testingData.length; i++)
            this.AdministratorDeleteServicesTemplate((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
    }


    // Ancillary methods ------------------------------------------------------

    protected void AdministratorDeleteServicesTemplate(final String beanName,String serviceBean,final Class<?> expected) {
        Class<?> caught;
        caught = null;
        try {
            Servicee ser= serviceeService.findOne(super.getEntityId(serviceBean));
            authenticate(beanName);
            serviceeService.cancel(ser);
            serviceeService.flush();
            unauthenticate();

        } catch (final Throwable oops) {
            caught = oops.getClass();
        }

        this.checkExceptions(expected, caught);
    }







    //Tarea	AE-206
    // Manage his or her services, which includes listing them, creating them, updating them, and deleting them as long as they are not required by any rendezvouses.
// kawtar hexo

    @Test
    public void managerCreateServicesManageDriver() {
        // String beanName,final String name, final String description, String url,String category
        final Object testingData[][] = {
                {
                        //crear servicio siendo logeado como manager1 ->true

                        "manager1", "adsfadsf", "fsadfsdaf", "http://www.genericPerril.net/posts/", "category1", null
                },
                {
                        "manager2", "adsfadsf", "fsadfsdaf", "http://www.genericPerril.net/posts/", "category1", null
                },
                {
                        "manager3", "kakakaka", "fsadfsdaf", "http://www.genericPerril.net/posts/", "category1", null
                },
                {
                        "manager1", "adsfadsf", "fsadfsdaf", "http://www.genericPerril.net/posts/", "category1", null
                },
                {
                        "manager1", "adsfadsf", "jdjdjdj", "http://www.genericPerril.net/posts/", "category1", null
                },
                {
                        //crear servicio sin proporcionar la url ->false

                        "manager1", "adsfadsf", "jdjdjdj", null, "category1", ConstraintViolationException.class
                },
                {
                    //crear servicio sin autentificar ->false
                        null, "adsfadsf", "jdjdjdj", "http://www.genericPerril.net/posts/", "category1", IllegalArgumentException.class
                },
                {
                        //crear servicio sin autentificar ->false
                        "manager4", "adsfadsf", "jdjdjdj", "http://www.genericPerril.net/posts/", null, AssertionError.class
                },



        };

        for (int i = 0; i < testingData.length; i++)
            this.managerCreateServicesManageTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Class<?>) testingData[i][5]);
    }


    // Ancillary methods ------------------------------------------------------

    protected void managerCreateServicesManageTemplate(final String beanName,final String name, final String description, String url,String category,final Class<?> expected) {
        Class<?> caught;
        caught = null;
        try {
            Category cat= categoryService.findOne(super.getEntityId(category));
            authenticate(beanName);
            Manager manager = managerService.findByPrincipal();
            Servicee servicee=serviceeService.create();
            servicee.setName(name);
            servicee.setDescription(description);
            servicee.setOwner(manager);
            servicee.setUrl(url);
            servicee.setCategorie(cat);
            serviceeService.save(servicee);
            serviceeService.flush();

        } catch (final Throwable oops) {
            caught = oops.getClass();
        }

        this.checkExceptions(expected, caught);
    }
   // delete servicio hexooooo kaw
   @Test
   public void managerDeleteServicesManageDriver() {
       // String beanName,final String name, final String description, String url,String category
       final Object testingData[][] = {
               {
                       //crear servicio siendo logeado como manager1 ->true

                       "manager1", "servicee1", null
               },
               {
                       "manager1", "servicee2", null               },
               {
                       "manager2", "servicee3", null               },
               {
                       "manager1", "servicee3", IllegalArgumentException.class               },
              {
                        null, "servicee2", IllegalArgumentException.class            },

               {
                       "manager1", null, AssertionError.class            },



       };

       for (int i = 0; i < testingData.length; i++)
           this.managerDeleteServicesManageTemplate((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
   }


    // Ancillary methods ------------------------------------------------------

    protected void managerDeleteServicesManageTemplate(final String beanName,String serviceBean,final Class<?> expected) {
        Class<?> caught;
        caught = null;
        try {
            Servicee ser= serviceeService.findOne(super.getEntityId(serviceBean));
            authenticate(beanName);
            serviceeService.delete(ser);
            serviceeService.flush();

            unauthenticate();

        } catch (final Throwable oops) {
            caught = oops.getClass();
        }

        this.checkExceptions(expected, caught);
    }






    // Tarea	AE-205
   //List the services that are available in the system
   @Test
   public void managerlistServicesManageDriver() {
      final Object testingData[][] = {
              {
                      "manager2", "adsfadsf", "fsadfsdaf", null
              },
              {
                      "manager1", "aaaaaa", "aaaaaa", null
              },
              {
                      "manager2", "sssss", "ssssss", null
              },
              {
                      "manager1", "sddddd", "asddddddddfsdffds", null
              },
              {
                      "manager2", "namdsffdse1", "asdfsdffds", null
              },
              {
                      "manager3", "ddsdas", "asdfsdffds", null
              },
              {
                      "manager3", null, "descripfsdfsdftion1", null
              },
              {
                      "manager4", null, "sdfdsfsdf", IllegalArgumentException.class
              },
              {
                      "user1", null, null, IllegalArgumentException.class
              },
              {
                      "manager2w", "name2", null, IllegalArgumentException.class
              },
              {      "userAccount3", null, "description2", IllegalArgumentException.class

              },
              {      "administrator1", null, "sdafsdfsda", IllegalArgumentException.class
              }
      };

      for (int i = 0; i < testingData.length; i++)
         this.managerlistServicesManageTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
   }


   // Ancillary methods ------------------------------------------------------

   protected void managerlistServicesManageTemplate(final String beanName, final String name, final String description,final Class<?> expected) {
      Class<?> caught;
      caught = null;
      try {
         authenticate(beanName);
         Manager manager = managerService.findByPrincipal();
         List<Servicee> servicees = new ArrayList<>( serviceeService.findAll());
         Servicee servicee = servicees.get(0);
         servicee.setName(name);
         servicee.setDescription(description);
         servicee.setOwner(manager);
         servicee.setUrl("http://www.genericPerril.net/posts/");
         serviceeService.save(servicee);
         Assert.notEmpty(servicees);

      } catch (final Throwable oops) {
         caught = oops.getClass();
      }

      this.checkExceptions(expected, caught);
   }

   @Test
   public void userlistServicesManageDriver() {
      final Object testingData[][] = {
              {
                      "user1", "adsfadsf", "fsadfsdaf", null
              },
              {
                      "user2", "aaaaaa", "aaaaaa", null
              },
              {
                      "user1", "sssss", "ssssss", null
              },
              {
                      "user3", "sddddd", "asddddddddfsdffds", null
              },
              {
                      "user3", "namdsffdse1", "asdfsdffds", null
              },
              {
                      "user4", "ddsdas", "asdfsdffds", null
              },
              {
                      "user2", null, "descripfsdfsdftion1", null
              },
              {
                      "manager4", null, "sdfdsfsdf", IllegalArgumentException.class
              },
              {
                      "ranger3", null, null, IllegalArgumentException.class
              },
              {
                      "manager2w", "name2", null, IllegalArgumentException.class
              },
              {      "userAccount3", null, "description2", IllegalArgumentException.class

              },
              {      "administrator1", null, "sdafsdfsda", IllegalArgumentException.class
              }
      };

      for (int i = 0; i < testingData.length; i++)
         this.userlistServicesManageTemplate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
   }


   // Ancillary methods ------------------------------------------------------

   protected void userlistServicesManageTemplate(final String beanName, final String name, final String description,final Class<?> expected) {
      Class<?> caught;
      caught = null;
      try {
         authenticate(beanName);
         User user = userService.findByPrincipal();
         List<Servicee> servicees = new ArrayList<>( serviceeService.findAll());
         Assert.notEmpty(servicees);

      } catch (final Throwable oops) {
         caught = oops.getClass();
      }

      this.checkExceptions(expected, caught);
   }
   // Tarea	AE-204
   //Request a service for one of the rendezvouses that he or she?s created. He or she must specify a valid credit card in every request for a service.
    // Optionally, he or she can provide some comments in the request.
//TODO kawtar


    @Test
    public void CreateService() {
        final Object testingData[][] = {
                {
                        "user1", 0 ,"comment 1" ,null
                },
                {
                        "user1", 1 ,"comment 2" ,null
                },

                {
                        "user2", 2,"comment 3" ,null
                },

                {
                        "user5", 0 ,"Comentario 1" , IllegalArgumentException.class
                },

                {
                        null, 0 ,"Comentario 0" , IllegalArgumentException.class
                },

                {
                        null, 1,"mi primera category" ,IllegalArgumentException.class
                },
                {
                        "administrator1", 0,null ,IllegalArgumentException.class
                },

        };

        for (int i = 0; i < testingData.length; i++)
            this.serviceCreateTemplate((String) testingData[i][0], (int) testingData[i][1],(String) testingData[i][2],(Class<?>) testingData[i][3]);
    }


    // Ancillary methods ------------------------------------------------------

    protected void serviceCreateTemplate(String beanName,int n, String comment, final Class<?> expected) {
        Class<?> caught;
        caught = null;
        try {
            authenticate(beanName);

            List<Servicee> servicees = new ArrayList<>(serviceeService.findAll());
            Servicee servicee = servicees.get(n);

            List<Rendezvous> rendezvous = new ArrayList<>(userService.findByPrincipal().getCreatedRendez());
            Rendezvous rendezvous1 = rendezvous.get(0);

            Assert.isTrue(creditCardService.checkCreditCard(userService.findByPrincipal().getCreditCard()));


            RequestForm requestForm = requestFormService.create();
            User user=userService.findByPrincipal();
            requestForm.setServicee(servicee);
            requestForm.setComment(comment);
            requestForm.setRendezvous(rendezvous1);

            requestForm.getServicee().getRendezvous().add(requestForm.getRendezvous());
            requestForm.getRendezvous().getServicees().add(requestForm.getServicee());
            requestFormService.save(requestForm);



            unauthenticate();
        } catch (final Throwable oops) {
            caught = oops.getClass();
        }

        this.checkExceptions(expected, caught);
    }










}